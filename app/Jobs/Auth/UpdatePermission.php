<?php

namespace App\Jobs\Auth;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldUpdate;
use App\Models\Auth\Permission;
use Illuminate\Support\Facades\DB;

class UpdatePermission extends Job implements ShouldUpdate
{
    public function handle(): Permission
    {
        DB::transaction(function () {
            $this->model->update($this->request->all());
            //print('x');
            
        });
        //var_dump($this->model);
        return $this->model;
        //var_dump($this->model);
        
    }
}