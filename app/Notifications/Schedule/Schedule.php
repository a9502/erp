<?php

namespace App\Notifications\Schedule;

use App\Abstracts\Notification;
use Modules\BulkEmail\Models\Template;

class Schedule extends Notification
{
    /**
     * The member model.
     *
     * @var object
     */
    public $member;

    /**
     * The email template.
     *
     * @var string
     */
    public $template;

    /**
     * Create a notification instance.
     *
     * @param  object  $bill
     * @param  object  $template_alias
     */
    public function __construct($member = null, $id = null)
    {
        //parent::__construct();
        $this->onQueue('notifications');

        $this->member = $member;
        $this->template = Template::where('id',$member->scheduling->template_id)->first();
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = $this->initMessage($this->member);
        return $message;
    }
 
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //'template_alias' => $this->template->alias
        ];
    }

    public function getTags()
    {
        return [
            '{name}',
            '{position}',
            '{schedule}',
            '{username}',
            '{password}',
            '{start_time}',
            '{end_time}',
            '{zoom_link}',
        ];
    }

    public function getTagsReplacement()
    {
        return [
            $this->member->name,
            $this->member->position,
            $this->member->schedule,
            $this->member->username,
            $this->member->password,
            $this->member->start_time,
            $this->member->end_time,
            $this->member->zoom_link
        ];
    }

}