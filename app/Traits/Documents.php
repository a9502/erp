<?php

namespace App\Traits;
use App\Models\Document\Document;
use Illuminate\Support\Str;

trait Documents
{
    public function getNextDocumentNumber(string $type): string
    {
        if ($alias = config('type.' . $type . '.alias')) {
            $type = $alias . '.' . str_replace('-', '_', $type);
        }

        $prefix = setting("$type.number_prefix");
        $next = setting("$type.number_next");
        $digit = setting("$type.number_digit");

        return $prefix . str_pad($next, $digit, '0', STR_PAD_LEFT);
    }
    
    protected function getSettingKey($type, $setting_key)
    {
        $key = '';
        $alias = config('type.' . $type . '.alias');

        if (!empty($alias)) {
            $key .= $alias . '.';
        }

        $prefix = config('type.' . $type . '.setting.prefix');
        $key .= $prefix . '.' . $setting_key;

        return $key;
    }

    public function getDocumentFileName(Document $document, string $separator = '-', string $extension = 'pdf'): string
    {
        return $this->getSafeDocumentNumber($document, $separator) . $separator . time() . '.' . $extension;
    }

    public function getSafeDocumentNumber(Document $document, string $separator = '-'): string
    {
        return Str::slug($document->document_number, $separator, language()->getShortCode());
    }

    public function increaseNextDocumentNumber(string $type): void
    {
        if ($alias = config('type.' . $type . '.alias')) {
            $type = $alias . '.' . str_replace('-', '_', $type);
        }

        $next = setting("$type.number_next", 1) + 1;

        setting(["$type.number_next" => $next]);
        setting()->save();
    }
}