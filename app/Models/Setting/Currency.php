<?php

namespace App\Models\Setting;

use App\Abstracts\Model;
use App\Traits\Contacts;
use App\Models\Document\Document;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Currency extends Model
{
    use Contacts, HasFactory;

    protected $table = 'currencies';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'name',
        'code',
        'rate',
        'enabled',
        'precision',
        'symbol',
        'symbol_first',
        'decimal_mark',
        'thousands_separator',
        'created_from',
        'created_by',
    ];

     /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'rate' => 'double',
        'enabled' => 'boolean',
    ];

     /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'code', 'rate', 'enabled'];

    /**
     * Scope to only include active models.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEnabled($query)
    {
        return $query->where($this->qualifyColumn('enabled'), 1);
    }

    /**
     * Scope currency by code.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $code
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCode($query, $code)
    {
        return $query->where($this->qualifyColumn('code'), $code);
    }

    public function accounts()
    {
        return $this->hasMany('App\Models\Banking\Account', 'currency_code', 'code');
    }

    public function customers()
    {
        return $this->contacts()->whereIn('type', (array) $this->getCustomerTypes());
    }

    public function invoices()
    {
        return $this->documents()->where('type', Document::INVOICE_TYPE);
    }

    public function bills()
    {
        return $this->documents()->where('type', Document::BILL_TYPE);
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Banking\Transaction', 'currency_code', 'code');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Common\Contact', 'currency_code', 'code');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document\Document', 'currency_code', 'code');
    }
}