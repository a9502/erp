<?php

namespace App\Models\Banking;

use App\Abstracts\Model;
use App\Traits\Transactions;
use Bkwld\Cloner\Cloneable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Account extends Model
{
    use Transactions,  Cloneable, HasFactory;

    protected $table = 'accounts';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['balance'];

     /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'name', 'number', 'currency_code', 'opening_balance',
         'bank_name', 'bank_phone', 'bank_address', 'enabled', 'created_from', 'created_by'];

    
    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'number', 'opening_balance', 'enabled'];

     /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'opening_balance' => 'double',
        'enabled' => 'boolean',
    ];

    /**
     * Get the current balance.
     *
     * @return string
     */
    public function getBalanceAttribute()
    {
        // Opening Balance
        $total = $this->opening_balance;

        // Sum Incomes
        $total += $this->income_transactions->sum('amount');

        // Subtract Expenses
        $total -= $this->expense_transactions->sum('amount');

        return $total;
    }

    /**
     * Get the current balance.
     *
     * @return string
     */
    public function getIncomeBalanceAttribute()
    {
        // Opening Balance
        //$total = $this->opening_balance;
        $total = 0;

        // Sum Incomes
        $total += $this->income_transactions->sum('amount');

        return $total;
    }

    /**
     * Get the current balance.
     *
     * @return string
     */
    public function getExpenseBalanceAttribute()
    {
        // Opening Balance
        //$total = $this->opening_balance;
        $total = 0;

        // Subtract Expenses
        $total += $this->expense_transactions->sum('amount');

        return $total;
    }



    public function income_transactions()
    {
        return $this->transactions()->whereIn('type', (array) $this->getIncomeTypes());
    }

    public function expense_transactions()
    {
        return $this->transactions()->whereIn('type', (array) $this->getExpenseTypes());
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Banking\Transaction');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Setting\Currency', 'currency_code', 'code');
    }

}