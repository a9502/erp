<?php

namespace App\Models\Auth;

use Laratrust\Models\LaratrustRole;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laratrust\Traits\LaratrustRoleTrait;
use Lorisleiva\LaravelSearchString\Concerns\SearchString;
use Kyslik\ColumnSortable\Sortable;

class Role extends LaratrustRole
{
    use HasFactory, LaratrustRoleTrait, SearchString, Sortable;

    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'display_name', 'description', 'created_from', 'created_by'];

    /**
     * Scope to get all rows filtered, sorted and paginated.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $sort
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCollect($query, $sort = 'display_name')
    {
        $request = request();
        $search = $request->get('search');
        $limit = (int) $request->get('limit', setting('default.list_limit', '25'));
        return $query->usingSearchString($search)->sortable($sort)->paginate($limit);
    }
}