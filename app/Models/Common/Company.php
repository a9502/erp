<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Laratrust\Contracts\Ownable;
use App\Events\Common\CompanyMakingCurrent;
use App\Utilities\Overrider;
use App\Events\Common\CompanyMadeCurrent;
use Lorisleiva\LaravelSearchString\Concerns\SearchString;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Events\Common\CompanyForgettingCurrent;
use App\Events\Common\CompanyForgotCurrent;
use App\Traits\Media;
use App\Models\Document\Document;

class Company extends Eloquent implements Ownable
{
    use Media, SearchString, SoftDeletes, Sortable;

    protected $table = 'companies';

    protected $fillable = ['domain', 'enabled', 'created_from', 'created_by'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'domain', 'email', 'enabled', 'created_at'];

    protected $casts = [
        'enabled' => 'boolean',
    ];

    public static function getCurrent()
    {
        if (!app()->has(static::class)) {
            return null;
        }

        return app(static::class);
    }

     /**
     * Scope to only include companies of a given user id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $user_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUserId($query, $user_id)
    {
        return $query->whereHas('users', function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        });
    }

    public function users()
    {
        return $this->morphedByMany('App\Models\Auth\User', 'user', 'user_companies', 'company_id', 'user_id');
    }

    public static function boot()
    {
        parent::boot();

        static::retrieved(function($model) {
            $model->setCommonSettingsAsAttributes();
        });

        static::saving(function($model) {
            $model->unsetCommonSettingsFromAttributes();
        });
    }

    public function setCommonSettingsAsAttributes()
    {
        $settings = $this->settings;

        //var_dump($settings);

        $groups = [
            'company',
            'default',
        ];

        foreach ($settings as $setting) {
            list($group, $key) = explode('.', $setting->getAttribute('key'));

            // Load only general settings
            if (!in_array($group, $groups)) {
                continue;
            }

            $value = $setting->getAttribute('value');

            if (($key == 'logo') && empty($value)) {
                $value = '/img/company.png';
            }

            $this->setAttribute($key, $value);
        }

        // Set default default company logo if empty
        if ($this->getAttribute('logo') == '') {
            $this->setAttribute('logo', 'img/company.png');
        }
    }

    public function unsetCommonSettingsFromAttributes()
    {
        $settings = $this->settings;

        $groups = [
            'company',
            'default',
        ];

        foreach ($settings as $setting) {
            list($group, $key) = explode('.', $setting->getAttribute('key'));

            // Load only general settings
            if (!in_array($group, $groups)) {
                continue;
            }

            $this->offsetUnset($key);
        }

        $this->offsetUnset('logo');
    }

    public function makeCurrent($force = false)
    {
        if (!$force && $this->isCurrent()) {
            return $this;
        }

        static::forgetCurrent();
        event(new CompanyMakingCurrent($this));

        // Bind to container
        app()->instance(static::class, $this);

        // Set session for backward compatibility @deprecated
        //session(['company_id' => $this->id]);

        // Load settings
        setting()->setExtraColumns(['company_id' => $this->id]);
        setting()->forgetAll();
        setting()->load(true);

        // Override settings and currencies
        Overrider::load('settings');
        Overrider::load('currencies');

        event(new CompanyMadeCurrent($this));
        return $this;

    }

    public function isCurrent()
    {
        return optional(static::getCurrent())->id === $this->id;
    }

    public static function forgetCurrent()
    {
        $current = static::getCurrent();

        if (is_null($current)) {
            return null;
        }

        event(new CompanyForgettingCurrent($current));

        // Remove from container
        app()->forgetInstance(static::class);

        // Unset session for backward compatibility @deprecated
        //session()->forget('company_id');

        // Remove settings
        setting()->forgetAll();

        event(new CompanyForgotCurrent($current));

        return $current;
    }

    /**
     * Scope to only include companies of a given enabled value.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEnabled($query, $value = 1)
    {
        return $query->where('enabled', $value);
    }

    public function settings()
    {
        return $this->hasMany('App\Models\Setting\Setting');
    }

    public function ownerKey($owner)
    {
        if ($this->isNotOwnable()) {
            return 0;
        }

        return $this->created_by;
    }

    /**
     * Scope to get all rows filtered, sorted and paginated.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $sort
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCollect($query, $sort = 'name')
    {
        $request = request();

        $search = $request->get('search');

        $query->usingSearchString($search)->sortable($sort);

        if ($request->expectsJson() && $request->isNotApi()) {
            return $query->get();
        }

        $limit = (int) $request->get('limit', setting('default.list_limit', '25'));

        return $query->paginate($limit);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document\Document::class);
    }

    public function bills()
    {
        return $this->documents()->where('type', Document::BILL_TYPE);
    }

    /**
     * Sort by company name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $direction
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function nameSortable($query, $direction)
    {
        return $query->join('settings', 'companies.id', '=', 'settings.company_id')
            ->where('key', 'company.name')
            ->orderBy('value', $direction)
            ->select('companies.*');
    }

    
}