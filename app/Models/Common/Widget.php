<?php

namespace App\Models\Common;

use App\Abstracts\Model;

class Widget extends Model
{
    protected $table = 'widgets';
}