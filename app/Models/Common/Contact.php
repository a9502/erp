<?php

namespace App\Models\Common;

use App\Abstracts\Model;
use Bkwld\Cloner\Cloneable;
use App\Models\Document\Document;
use App\Traits\Contacts;
use App\Traits\Currencies;

class Contact extends Model
{
    use Cloneable, Contacts, Currencies;

    protected $table = 'contacts';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['location'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'type',
        'name',
        'email',
        'user_id',
        'tax_number',
        'phone',
        'address',
        'city',
        'zip_code',
        'state',
        'country',
        'website',
        'currency_code',
        'reference',
        'enabled',
        'created_from',
        'created_by',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'enabled' => 'boolean',
    ];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'email', 'phone', 'enabled'];


    /**
     * Scope to include only vendors.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVendor($query)
    {
        return $query->whereIn($this->qualifyColumn('type'), (array) $this->getVendorTypes());
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document\Document');
    }

    public function bills()
    {
        return $this->documents()->where('type', Document::BILL_TYPE);
    }

    public function getUnpaidAttribute()
    {
        $amount = 0;
        $collection = $this->isCustomer() ? 'invoices' : 'bills';

        $this->$collection->whereNotIn('status', ['draft', 'cancelled', 'paid'])->each(function ($item) use (&$amount) {
            $amount += $this->convertToDefault($item->amount_due, $item->currency_code, $item->currency_rate);
        });

        return $amount;
    }

    public function getLocationAttribute()
    {
        $location = [];

        if ($this->city) {
            $location[] = $this->city;
        }

        if ($this->zip_code) {
            $location[] = $this->zip_code;
        }

        if ($this->state) {
            $location[] = $this->state;
        }

        if ($this->country) {
            $location[] = trans('countries.' . $this->country);
        }

        return implode(', ', $location);
    }

    public function invoices()
    {
        return $this->documents()->where('type', Document::INVOICE_TYPE);
    }

    /**
     * Scope to include only customers.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCustomer($query)
    {
        return $query->whereIn($this->qualifyColumn('type'), (array) $this->getCustomerTypes());
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Banking\Transaction');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'user_id', 'id');
    }

}