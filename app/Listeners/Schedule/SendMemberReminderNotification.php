<?php

namespace App\Listeners\Schedule;

use App\Events\Schedule\MemberReminded as Event;
use App\Models\Auth\User;

class SendMemberReminderNotification
{
    /**
     * Handle the event.
     *
     * @param  $event
     * @return array
     */
    public function handle(Event $event)
    {
        $member = $event->member;
        //$user = User::find(1);
        $notification = $event->notification;
        //$user->notify(new $notification($member, $event->member->scheduling->template_id));
        $member->notify(new $notification($member, $event->member->scheduling->template_id));
        //new $notification($member, $event->member->scheduling->template_id);
    }
}

