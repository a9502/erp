<?php

namespace App\Events\Schedule;

use App\Abstracts\Event;
use Modules\BulkEmail\Models\Member;

class MemberReminded extends Event
{
    public $member;
    public $notification;

    /**
     * Create a new event instance.
     *
     * @param $document
     * @param $notification
     */
    public function __construct(Member $member, string $notification)
    {
        $this->member = $member;
        $this->notification = $notification;
    }
}
