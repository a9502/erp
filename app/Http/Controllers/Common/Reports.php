<?php

namespace App\Http\Controllers\Common;


use App\Abstracts\Http\Controller;
use App\Http\Requests\Common\Report as Request;
use App\Jobs\Common\CreateReport;
use App\Jobs\Common\DeleteReport;
use App\Jobs\Common\UpdateReport;
use App\Models\Common\Report;
use App\Utilities\Reports as Utility;
use Illuminate\Support\Facades\Cache;

class Reports extends Controller
{
    /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        // Add CRUD permission check
        $this->middleware('permission:create-common-reports')->only('create', 'store', 'duplicate', 'import');
        $this->middleware('permission:read-common-reports')->only('index', 'show', 'export');
        $this->middleware('permission:update-common-reports')->only('edit', 'update', 'enable', 'disable');
        $this->middleware('permission:delete-common-reports')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $totals = $icons = $categories = [];
        $reports = Report::orderBy('name')->get();

        foreach ($reports as $report) 
        {
            
            if (!Utility::canShow($report->class)) {
                continue;
            }

            $class = Utility::getClassInstance($report, false);
            
            if (empty($class)) 
            {
                continue;
            }

            

            $ttl = 3600 * 6; // 6 hours
            //echo $ttl;
            /*$totals[$report->id] = Cache::remember('reports.totals.' . $report->id, $ttl, function () use ($class,$report) {
                echo $report;
                //echo 'xx :'.$class->getGrandTotal();
                //return $class->getGrandTotal();
            *});*/

            $totals[$report->id]  = $class->getGrandTotal();
            $icons[$report->id] = $class->getIcon();
            $categories[$class->getCategory()][] = $report;
        }
        
        return $this->response('common.reports.index', compact('categories', 'totals', 'icons'));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @param  Report $report
     * @return Response
     */
    public function show(Report $report)
    {
        if (!Utility::canShow($report->class)) {
            abort(403);
        }

        $class = Utility::getClassInstance($report);
        // Update cache
        Cache::put('reports.totals.' . $report->id, $class->getGrandTotal());
        return $class->show();
    }

    /**
     * Clear the cache of the resource.
     *
     * @return Response
     */
    public function clear(Report $report)
    {
        $data = Utility::getClassInstance($report)->getGrandTotal();
        Cache::put('reports.totals.' . $report->id, $data);

        return response()->json([
            'success' => true,
            'error' => false,
            'data' => $data,
            'message' => '',
        ]);
    }
}