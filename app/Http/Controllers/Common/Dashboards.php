<?php

namespace App\Http\Controllers\Common;

use App\Abstracts\Http\Controller;
use App\Models\Common\Dashboard;
use App\Models\Common\Widget;
use App\Utilities\Widgets;
use App\Traits\DateTime;
use App\Traits\Users;

class Dashboards extends Controller
{
    use DateTime, Users;

    /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        // Add CRUD permission check
        $this->middleware('permission:create-common-dashboards')->only('create', 'store', 'duplicate', 'import');
        $this->middleware('permission:read-common-dashboards')->only('show');
        $this->middleware('permission:update-common-dashboards')->only('index', 'edit', 'export', 'update', 'enable', 'disable', 'share');
        $this->middleware('permission:delete-common-dashboards')->only('destroy');
    }

    /** 
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show( $dashboard_id = null )
    {
        $dashboard_id = $dashboard_id ?? session('dashboard_id');
        $dashboard = Dashboard::find($dashboard_id);
        if(! $dashboard) {
            $dashboard = user()->dashboards()->enabled()->first();
        }

        //echo $dashboard;
        if (empty($dashboard)) {
            $dashboard = $this->dispatch(new CreateDashboard([
                'company_id' => company_id(),
                'name' => trans_choice('general.dashboards', 1),
                'default_widgets' => 'core',
            ]));
        }

        session(['dashboard_id' => $dashboard->id]);

        $widgets = Widget::where('dashboard_id', $dashboard->id)
            ->orderBy('sort', 'asc')
            ->get()
            ->filter(function ($widget) {
                return Widgets::canShow($widget->class);
            });

        $date_picker_shortcuts = $this->getDatePickerShortcuts();

        if (!request()->has('start_date')) {
            request()->merge(['start_date' => $date_picker_shortcuts[trans('reports.this_year')]['start']]);
            request()->merge(['end_date' => $date_picker_shortcuts[trans('reports.this_year')]['end']]);
        }

        return view('common.dashboards.show', compact('dashboard', 'widgets', 'date_picker_shortcuts'));
    }


    public function show_old($dashboard_id = null)
    {
        echo 'x';
        $dashboard_id = $dashboard_id ?? session('dashboard_id');
        try {
            
            $dashboard = Dashboard::findOrFail($dashboard_id);
            echo $dashboard;
        } catch (ModelNotFoundException $e) {
            //echo 'x';
            $dashboard = user()->dashboards()->enabled()->first();
            echo $dashboard;
        }

        echo $dashboard;

        //echo user()->dashboards()->enabled()->first();

        if (empty($dashboard)) {
            $dashboard = $this->dispatch(new CreateDashboard([
                'company_id' => company_id(),
                'name' => trans_choice('general.dashboards', 1),
                'default_widgets' => 'core',
            ]));
        }

        session(['dashboard_id' => $dashboard->id]);

        $widgets = Widget::where('dashboard_id', $dashboard->id)
            ->orderBy('sort', 'asc')
            ->get()
            ->filter(function ($widget) {
                return Widgets::canShow($widget->class);
            });

        $date_picker_shortcuts = $this->getDatePickerShortcuts();

        if (!request()->has('start_date')) {
            request()->merge(['start_date' => $date_picker_shortcuts[trans('reports.this_year')]['start']]);
            request()->merge(['end_date' => $date_picker_shortcuts[trans('reports.this_year')]['end']]);
        }

        return view('common.dashboards.show', compact('dashboard', 'widgets', 'date_picker_shortcuts'));
    }

    /**
     * Change the active dashboard.
     *
     * @param  Dashboard  $dashboard
     *
     * @return Response
     */
    public function switch(Dashboard $dashboard)
    {
        if ($this->isUserDashboard($dashboard->id)) {
            session(['dashboard_id' => $dashboard->id]);
        }

        return redirect()->route('dashboard');
    }


}