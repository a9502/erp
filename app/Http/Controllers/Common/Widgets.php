<?php

namespace App\Http\Controllers\Common;

use App\Abstracts\Http\Controller;
use App\Utilities\Widgets as Utility;

class Widgets extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $widgets = Utility::getClasses('all');

        return response()->json($widgets);
    }
    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return redirect()->route('dashboard');
    }
}