<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class Header
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = user();

        $view->with([
            'user' => $user,
        ]);
    }
}