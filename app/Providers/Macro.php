<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\View\Factory as ViewFactory;

class Macro extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

     /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Request::macro('isApi', function () {
            return $this->is(config('api.subtype') . '/*');
        });

        Request::macro('isSigned', function ($company_id) {
            return $this->is($company_id . '/signed/*');
        });

        Request::macro('isNotSigned', function ($company_id) {
            return !$this->isSigned($company_id);
        });

        Request::macro('isNotApi', function () {
            return !$this->isApi();
        });

        Request::macro('isInstall', function () {
            return $this->is('install/*');
        });

        Str::macro('filename', function ($string, $separator = '-') {
            // Replace @ with the word 'at'
            $string = str_replace('@', $separator.'at'.$separator, $string);
            // Remove all characters that are not the separator, letters, numbers, or whitespace.
            $string = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', $string);
            // Remove multiple whitespaces
            $string = preg_replace('/\s+/', ' ', $string);

            return $string;
        });
    }

}