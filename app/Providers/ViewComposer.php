<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider as Provider;
use View;

class ViewComposer extends Provider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Add limits and bulk actions to index
        View::composer(
            '*.index', 'App\Http\ViewComposers\Index'
        );

        // Add limits to show
        View::composer(
            '*.show', 'App\Http\ViewComposers\Index'
        );

        // Add notifications to header
        View::composer(
            ['partials.wizard.navbar', 'partials.admin.navbar', 'partials.portal.navbar'],
            'App\Http\ViewComposers\Header'
        );

        // Add company info to menu
        //, 'partials.portal.menu'
        View::composer(
            ['partials.admin.menu'],
            'App\Http\ViewComposers\Menu'
        );

         // Wizard
        View::composer(
            'layouts.wizard', 'App\Http\ViewComposers\Wizard'
        );

        // Add recurring
        View::composer(
            'partials.form.recurring', 'App\Http\ViewComposers\Recurring'
        );
    }
}

