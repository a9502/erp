<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as Provider;

class Event extends Provider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Menu\AdminCreated' => [
            'App\Listeners\Menu\AddAdminItems',
        ],

        'App\Events\Auth\LandingPageShowing' => [
            'App\Listeners\Auth\AddLandingPages',
        ],

        'App\Events\Module\Installed' => [
            'App\Listeners\Module\InstallExtraModules',
            'App\Listeners\Module\FinishInstallation',
        ],
        'App\Events\Module\Uninstalled' => [
            'App\Listeners\Module\FinishUninstallation',
        ],
        'App\Events\Document\DocumentReminded' => [
            'App\Listeners\Document\SendDocumentReminderNotification',
        ],
        'App\Events\Schedule\MemberReminded' => [
            'App\Listeners\Schedule\SendMemberReminderNotification',
        ],
        'App\Events\Menu\PortalCreated' => [
            'App\Listeners\Menu\AddPortalItems',
        ],
        'App\Events\Document\DocumentCreated' => [
            'App\Listeners\Document\CreateDocumentCreatedHistory',
            'App\Listeners\Document\IncreaseNextDocumentNumber',
            'App\Listeners\Document\SettingFieldCreated',
        ],
        'App\Events\Document\DocumentCancelled' => [
            'App\Listeners\Document\MarkDocumentCancelled',
        ],
        
    ];
}