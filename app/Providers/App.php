<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class App extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (config('app.debug')) {
            //echo 'test debug';
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            //var_dump($test);
        }

        if (config('app.env') !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Laravel db fix
        Schema::defaultStringLength(191);
        Paginator::useBootstrap();
        Model::preventLazyLoading(config('app.eager_load'));
        Model::handleLazyLoadingViolationUsing(function ($model, $relation) {
            $class = get_class($model);

            report("Attempted to lazy load [{$relation}] on model [{$class}].");
        });

    }
}
