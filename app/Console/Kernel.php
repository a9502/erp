<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Modules\BulkEmail\Models\Schedule as Model;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       // \App\Console\Commands\SendEmails::class,
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $records = Model::enabled()
            ->today()
            ->get();

        if( ! $records)
            return; 

        //print ('records :'. $records->count());
        
        foreach($records as $record)
        {
            $inHour = \Carbon\Carbon::parse($record->schedule_at)->format('H:i');
            $schedule->command('send:mail',[$record->id])->dailyAt($inHour);  
        }

        //$schedule->command('send:mail')->everyMinute();
        //$schedule->command('reminder:bill')->dailyAt($schedule_time);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return config('app.timezone');
    }
}
