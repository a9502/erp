<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;
use Mailgun\Mailgun;
use Mailgun\Exception\HttpClientException ;
use Illuminate\Support\Facades\Http;

class MailgunLog extends Command
{
    protected $output;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mailgun:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Log Mail for bulk mail mailgun';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->output = new ConsoleOutput();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::withBasicAuth('api', '36408c3de901a6c6e5ae84398fc5e504-e2e3d8ec-edf15ad7')
            ->get('https://api.mailgun.net/v3/mg.mail-assessment.com/events');

        $results  = $response->json();
        if($response->successful()) 
        {
            foreach($results['items'] as $res)
            {
                $this->output->writeln('recipient :' . $res['recipient'] );
            }
        }
    }

}