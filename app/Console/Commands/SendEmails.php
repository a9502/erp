<?php

namespace App\Console\Commands;

use Modules\BulkEmail\Models\Member;
use Modules\BulkEmail\Models\Schedule as Model;
use Modules\BulkEmail\Models\Template as Theme;

use Illuminate\Console\Command;
use App\Notifications\Schedule\Schedule as Notification;
use App\Events\Schedule\MemberReminded;
use Mail;
use App\Mail\MyDemoMail;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail {schedule}';

    /**
     * construct member
     */
    private $member;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Mail for bulk mail member';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $output = new ConsoleOutput();
        $scheduleId = $this->argument('schedule');
        $schedule = Model::find($scheduleId);
        $template = DB::table('bulk_email_templates')->where('id',$schedule->template_id)->first();

        if(! $schedule)
            return true;

        config(['laravel-model-caching.enabled' => false]);
        $members = Member::where('schedule_id',$scheduleId)->orderBy('email','asc')->get();
        $schedule->update(['status' => Model::PROCESS]);
        $total = count($members);
        foreach($members as $key => $member)
        {
            try {
                $this->member = $member;
                $output->writeln($this->member->name.' has been Execute key :'.($key + 1).'/'.$total );
                //event(new MemberReminded($member, Notification::class));

                $toMail = env('APP_ENV') == 'testing' ? 'hendarsyahss@gmail.com' : $member->email;
                
                
                Mail::to($toMail)
                    ->queue(new MyDemoMail([
                        'subject' => $this->getSubject($template->subject),
                        'body' => $this->getBody($template->body)
                    ]));
                    
                
                if(env('APP_ENV') == 'testing' )
                {
                    break;
                }

            } catch (\Throwable $e) {
                //print 'error, email not sent at :'.$member->name.' error :'.$e->getMessage();
                //$this->error($e->getMessage());
                //print 'error :'.$e->getMessage();
                //$output->writeln('error :'.$e->getMessage());
                $output->writeln($member->name.'has error : '.$e->getMessage().' at key : '.($key + 1) .'/'.$total);
                continue;

                //report($e);
            }
        }

        $schedule::where('id',$scheduleId)->update(['status' => Model::FINISH]);

        return true;
    }

    public function getSubject($content)
    {
        return $this->replaceTags($content);
    }

    public function getBody($content)
    {
        return $this->replaceTags($content);
    }

    public function replaceTags($content)
    {
        $pattern = $this->getTagsPattern();
        $replacement = $this->applyQuote($this->getTagsReplacement());

        return $this->revertQuote(preg_replace($pattern, $replacement, $content));
    }

    public function getTagsPattern()
    {
        $pattern = [];

        foreach($this->getTags() as $tag) {
            $pattern[] = "/" . $tag . "/";
        }

        return $pattern;
    }

    public function getTags()
    {
        return [
            '{name}',
            '{position}',
            '{schedule}',
            '{username}',
            '{password}',
            '{start_time}',
            '{end_time}',
            '{zoom_link}',
        ];
    }

    public function getTagsReplacement()
    {
        return [
            $this->member->name,
            $this->member->position,
            $this->member->schedule,
            $this->member->username,
            $this->member->password,
            $this->member->start_time,
            $this->member->end_time,
            $this->member->zoom_link
        ];
    }

    public function applyQuote($vars)
    {
        $new_vars = [];

        foreach ($vars as $var) {
            $new_vars[] = preg_quote($var);
        }

        return $new_vars;
    }

    public function revertQuote($content)
    {
        return str_replace('\\', '', $content);
    }
}
