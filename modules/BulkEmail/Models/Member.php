<?php

namespace Modules\BulkEmail\Models;

use Modules\BulkEmail\Models\Schedule;

use App\Abstracts\Model;
use Bkwld\Cloner\Cloneable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;

class Member extends Model 
{
    use HasFactory, Cloneable, Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bulk_email_members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['schedule_id','name','email','schedule','position','username','password',
        'zoom_link','start_time','end_time'];

    /**
     * Get the schedule that owns the phone.
     */
    public function scheduling()
    {
        return $this->belongsTo(Schedule::class,'schedule_id','id');
    }


}

