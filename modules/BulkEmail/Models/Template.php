<?php

namespace Modules\BulkEmail\Models;

use App\Abstracts\Model;
use Bkwld\Cloner\Cloneable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Template extends Model
{
    use HasFactory, Cloneable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bulk_email_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','company_id','type','subject','enabled','body',
        'created_from','updated_by','created_by'];

    public $types = array(
        'Test'      => 'Test',
        'Interview' => 'Interview',
        'Completenes' => 'Completenes',
        'Not Passing Administration Selection' => 'Not Passing Administration Selection',
        'Not Passing Online Test' => 'Not Passing Online Test'
    );
    /**
     * Scope to only include test type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTest($query)
    {
        return $query->where('type', \Modules\BulkEmail\Models\Schedule::TEST);
    }

    /**
     * Scope to only include test type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInterview($query)
    {
        return $query->where('type', \Modules\BulkEmail\Models\Schedule::INTERVIEW);
    }

    /**
     * Scope to only include test type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCompletenes($query)
    {
        return $query->where('type', \Modules\BulkEmail\Models\Schedule::COMPLETENES);
    }

    /**
     * Scope to only include test type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotPassingTest($query)
    {
        return $query->where('type', \Modules\BulkEmail\Models\Schedule::NOT_PASSING_TEST);
    }

    /**
     * Scope to only include test type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotPassingAdministration($query)
    {
        return $query->where('type', \Modules\BulkEmail\Models\Schedule::NOT_PASSING_ADMINISTRATION);
    }

    
}