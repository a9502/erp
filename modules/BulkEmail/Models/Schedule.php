<?php

namespace Modules\BulkEmail\Models;

use Modules\BulkEmail\Models\Template;

use App\Abstracts\Model;
use Bkwld\Cloner\Cloneable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class Schedule extends Model 
{
    const TEST = 'Test';
    const INTERVIEW = 'Interview';
    const COMPLETENES = 'Completenes';
    const NOT_PASSING_TEST = 'Not Passing Online Test';
    const NOT_PASSING_ADMINISTRATION = 'Not Passing Administration Selection';
    const DRAFT = 'draft';
    const PROCESS = 'process';
    const FINISH = 'finish';

    use HasFactory, Cloneable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bulk_email_schedules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','schedule_at','type','template_id','total_member','enabled',
        'created_from','updated_by','created_by','status'];

    /**
     * Relation to members model
     */
    public function members()
    {
        return $this->hasMany('Modules\BulkEmail\Models\Member');
    }

    /**
     * Get the schedule that owns the phone.
     */
    public function template()
    {
        return $this->belongsTo(Template::class,'template_id','id');
    }

    /**
     * Scope to only test.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTest($query)
    {
        return $query->where($this->qualifyColumn('type'), Schedule::TEST);
    }

    /**
     * Scope to only completeness.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCompletenes($query)
    {
        return $query->where($this->qualifyColumn('type'), Schedule::COMPLETENES);
    }

    /**
     * Scope to only not passing test.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotPassingTest($query)
    {
        return $query->where($this->qualifyColumn('type'), Schedule::NOT_PASSING_TEST);
    }

    /**
     * Scope to only not passing administration.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotPassingAdministration($query)
    {
        return $query->where($this->qualifyColumn('type'), Schedule::NOT_PASSING_ADMINISTRATION);
    }


    /**
     * Scope to only interview.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInterview($query)
    {
        return $query->where($this->qualifyColumn('type'), Schedule::INTERVIEW);
    }

    /**
     * Scope to only include today schedule.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeToday($query)
    {
        $today = date('Y-m-d');
        return $query->where(DB::raw("DATE_FORMAT(schedule_at,'%Y-%m-%d')"),$today);
    }


}

