<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBulkEmailInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulk_email_interviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email',100);
            $table->string('name',100);
            $table->string('schedule',100);
            $table->string('start_time',10);
            $table->string('end_time',10);
            $table->string('zoom_link',225);
            $table->string('username',100);
            $table->string('password',100);
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulk_email_interviews');
    }
}
