<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBulkEmailSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulk_email_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',150);
            $table->dateTime('schedule_at')->nullable();
            $table->string('type',25)->default('Test');
            $table->unsignedBigInteger('template_id');
            $table->unsignedBigInteger('total_member')->default(0);
            $table->boolean('enabled')->default(true);
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->string('created_from',100)->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->softDeletes();

            $table->foreign('template_id')
                ->references('id')
                ->on('bulk_email_templates')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
