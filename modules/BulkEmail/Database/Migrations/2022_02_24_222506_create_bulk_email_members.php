<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBulkEmailMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulk_email_members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('schedule_id');
            $table->string('email',100);
            $table->string('name',100);
            $table->string('position',100)->nullable();
            $table->string('schedule',100);
            $table->string('username',100);
            $table->string('password',100);
            $table->string('start_time',10)->nullable();
            $table->string('end_time',10)->nullable();
            $table->string('zoom_link',225)->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();

            $table->foreign('schedule_id')
                ->references('id')
                ->on('bulk_email_schedules')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
