<?php

namespace Modules\BulkEmail\Listeners;

use App\Events\Module\Installed as Event;
use App\Traits\Permissions;
use Artisan;

class FinishInstallation
{
    use Permissions;

    public $alias = 'bulk-email';

    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        //print ('tesst :');
        if ($event->alias != $this->alias) {
            return;
        }

        $this->updatePermissions();

        $this->callSeeds();
    }

    protected function updatePermissions()
    {
        // c=create, r=read, u=update, d=delete
        $this->attachPermissionsToAdminRoles([
            $this->alias . '-templates' => 'c,r,u,d',
            $this->alias . '-tests' => 'c,r,u,d',
            $this->alias . '-interviews' => 'c,r,u,d',
        ]);

        //$this->attachModuleWidgetPermissions($this->alias);
    }

    protected function callSeeds()
    {
        Artisan::call('company:seed', [
            'company' => company_id(),
            '--class' => 'Modules\BulkEmail\Database\Seeds\BulkEmailDatabaseSeeder',
        ]);
    }
}
