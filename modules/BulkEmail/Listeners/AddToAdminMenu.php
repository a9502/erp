<?php

namespace Modules\BulkEmail\Listeners;

use App\Events\Menu\AdminCreated;
use Modules\BulkEmail\Events\BEDropdownCreated;
use Modules\BulkEmail\Events\AddingBEDropdown;

class AddToAdminMenu
{
    /**
     * Handle the event.
     *
     * @param AdminCreated $event
     * @return void
     */
    public function handle(AdminCreated $event)
    {
        $user = user();

        $can_read_templates = $user->can('read-bulk-email-templates');
        $can_read_tests = $user->can('read-bulk-email-tests');
        $can_read_interviews = $user->can('read-bulk-email-interviews');
        $show_dropdown = $can_read_templates || $can_read_tests || $can_read_interviews ;
        //$show_dropdown = true;

        if (!$show_dropdown) {
            event(new AddingBEDropdown($show_dropdown));
        }

        if (!$show_dropdown) {
            return;
        }

        $event->menu->dropdown(trans('bulk-email::general.bulk_email'), function ($sub) use ($can_read_templates, $can_read_tests,$can_read_interviews) {
            if ($can_read_tests) {
                $sub->route('bulk-email.tests.index', trans_choice('bulk-email::general.online_tests', 2), [], 10, []);
            }

            if ($can_read_interviews) {
                $sub->route('bulk-email.interviews.index', trans_choice('bulk-email::general.online_interviews', 2), [], 20, []);
            }

            if ($can_read_tests) {
                $sub->route('bulk-email.completeness.index', trans_choice('bulk-email::general.completeness', 2), [], 10, []);
            }

            if ($can_read_tests) {
                $sub->route('bulk-email.not-passing-test.index', trans_choice('bulk-email::general.not_passing_test', 2), [], 10, []);
            }

            if ($can_read_tests) {
                $sub->route('bulk-email.not-passing-administration.index', trans_choice('bulk-email::general.not_passing_administration', 2), [], 10, []);
            }

            if ($can_read_templates) {
                $sub->route('bulk-email.templates.index', trans_choice('bulk-email::general.templates', 2), [], 30, []);
            }

            if ($can_read_tests) {
                $sub->route('bulk-email.logs.index', trans_choice('bulk-email::general.logs', 2), [], 40, []);
            }

            event(new BEDropdownCreated($sub));
        }, 43, ['icon' => 'fa fa-envelope']);
    }
}