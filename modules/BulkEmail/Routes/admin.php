<?php

use Illuminate\Support\Facades\Route;

/**
 * 'admin' middleware and 'bulk-email' prefix applied to all routes (including names)
 *
 * @see \App\Providers\Route::register
 */

Route::admin('bulk-email', function () {
    Route::get('tests/{test}/duplicate', 'Tests@duplicate')->name('tests.duplicate');
    Route::get('tests/export', 'Tests@export')->name('tests.export');
    Route::post('tests/import', 'Tests@import')->name('tests.import');
    Route::get('tests/{test}/enable', 'Tests@enable')->name('tests.enable');
    Route::get('tests/{test}/disable', 'Tests@disable')->name('tests.disable');
    Route::delete('tests/members/{member}', 'Tests@memberDestroy')->name('tests.members.destroy');
    Route::resource('tests', 'Tests');

    Route::get('interviews/{interview}/duplicate', 'Interviews@duplicate')->name('interviews.duplicate');
    Route::get('interviews/export', 'Interviews@export')->name('interviews.export');
    Route::post('interviews/import', 'Interviews@import')->name('interviews.import');
    Route::get('interviews/{interview}/enable', 'Interviews@enable')->name('interviews.enable');
    Route::get('interviews/{interview}/disable', 'Interviews@disable')->name('interviews.disable');
    Route::delete('interviews/members/{member}', 'Interviews@memberDestroy')->name('interviews.members.destroy');
    Route::resource('interviews', 'Interviews');

    Route::get('completeness/{completenes}/duplicate', 'Completeness@duplicate')->name('completeness.duplicate');
    Route::get('completeness/export', 'Completeness@export')->name('completeness.export');
    Route::post('completeness/import', 'Completeness@import')->name('completeness.import');
    Route::get('completeness/{completenes}/enable', 'Completeness@enable')->name('completeness.enable');
    Route::get('completeness/{completenes}/disable', 'Completeness@disable')->name('completeness.disable');
    Route::delete('completeness/members/{member}', 'Completeness@memberDestroy')->name('completeness.members.destroy');
    Route::resource('completeness', 'Completeness');

    Route::get('not-passing-test/{item}/duplicate', 'NotPassingTests@duplicate')->name('not-passing-test.duplicate');
    Route::get('not-passing-test/export', 'NotPassingTests@export')->name('not-passing-test.export');
    Route::post('not-passing-test/import', 'NotPassingTests@import')->name('not-passing-test.import');
    Route::get('not-passing-test/{item}/enable', 'NotPassingTests@enable')->name('not-passing-test.enable');
    Route::get('not-passing-test/{item}/disable', 'NotPassingTests@disable')->name('not-passing-test.disable');
    Route::delete('not-passing-test/members/{member}', 'NotPassingTests@memberDestroy')->name('not-passing-test.members.destroy');
    Route::resource('not-passing-test', 'NotPassingTests');

    Route::get('not-passing-administration/{item}/duplicate', 'NotPassingAdministrations@duplicate')->name('not-passing-administration.duplicate');
    Route::get('not-passing-administration/export', 'NotPassingAdministrations@export')->name('not-passing-administration.export');
    Route::post('not-passing-administration/import', 'NotPassingAdministrations@import')->name('not-passing-administration.import');
    Route::get('not-passing-administration/{item}/enable', 'NotPassingAdministrations@enable')->name('not-passing-administration.enable');
    Route::get('not-passing-administration/{item}/disable', 'NotPassingAdministrations@disable')->name('not-passing-administration.disable');
    Route::delete('not-passing-administration/members/{member}', 'NotPassingAdministrations@memberDestroy')->name('not-passing-administration.members.destroy');
    Route::resource('not-passing-administration', 'NotPassingAdministrations');

    Route::get('templates/{template}/duplicate', 'Templates@duplicate')->name('templates.duplicate');
    Route::get('templates/{template}/enable', 'Templates@enable')->name('templates.enable');
    Route::get('templates/{template}/disable', 'Templates@disable')->name('templates.disable');
    Route::resource('templates', 'Templates');

    Route::resource('logs', 'Logs');
});
