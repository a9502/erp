<?php

namespace Modules\BulkEmail\Jobs\Test;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use Modules\BulkEmail\Models\Schedule as Test;
use Illuminate\Support\Facades\DB;

class CreateTest extends Job implements HasOwner, HasSource, ShouldCreate
{
    public function handle(): Test
    {
        //echo $this->request;
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model = Test::create($this->request->all());
        });

        return $this->model;
    }
}