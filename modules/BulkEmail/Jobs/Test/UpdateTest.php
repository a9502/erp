<?php

namespace Modules\BulkEmail\Jobs\Test;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldUpdate;
use Modules\BulkEmail\Models\Schedule as Test;
use Illuminate\Support\Facades\DB;

class UpdateTest extends Job implements HasOwner, ShouldUpdate
{
    public function handle(): Test
    {
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model->update($value);
        });

        return $this->model;
    }
}