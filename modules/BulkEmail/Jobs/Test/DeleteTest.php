<?php

namespace Modules\BulkEmail\Jobs\Test;

use App\Abstracts\Job;
use Modules\BulkEmail\Models\Schedule as Test;

class DeleteTest extends Job
{
    protected $test;

    public function __construct(Test $test)
    {
        $this->test = $test;
    }

    public function handle(): bool
    {
        $this->authorize();
        $this->test->delete();
        return true;
    }

    public function authorize()
    {
        if ($relationships = $this->getRelationships()) {
            $message = trans('messages.warning.deleted', ['name' => $this->test->name, 'text' => implode(', ', $relationships)]);

            throw new \Exception($message);
        }
    }

    public function getRelationships(): array
    {
        $rels = [
            'members' => 'bulk-email::general.members',
        ];

        return $this->countRelationships($this->test, $rels);
    }
}
