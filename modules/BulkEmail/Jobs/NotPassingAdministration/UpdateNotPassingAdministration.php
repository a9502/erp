<?php

namespace Modules\BulkEmail\Jobs\NotPassingAdministration;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldUpdate;
use Modules\BulkEmail\Models\Schedule as NotPassingAdministration;
use Illuminate\Support\Facades\DB;

class UpdateNotPassingAdministration extends Job implements HasOwner, ShouldUpdate
{
    public function handle(): NotPassingAdministration
    {
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model->update($value);
        });

        return $this->model;
    }
}