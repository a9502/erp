<?php

namespace Modules\BulkEmail\Jobs\NotPassingAdministration;

use App\Abstracts\Job;
use Modules\BulkEmail\Models\Schedule as NotPassingAdministration;

class DeleteNotPassingAdministration extends Job
{
    protected $notPassingAdministration;

    public function __construct(NotPassingAdministration $notPassingAdministration)
    {
        $this->notPassingAdministration = $notPassingAdministration;
    }

    public function handle(): bool
    {
        $this->authorize();
        $this->notPassingAdministration->delete();
        return true;
    }

    public function authorize()
    {
        if ($relationships = $this->getRelationships()) {
            $message = trans('messages.warning.deleted', ['name' => $this->notPassingAdministration->name, 'text' => implode(', ', $relationships)]);

            throw new \Exception($message);
        }
    }

    public function getRelationships(): array
    {
        $rels = [
            'members' => 'bulk-email::general.members',
        ];

        return $this->countRelationships($this->notPassingAdministration, $rels);
    }
}
