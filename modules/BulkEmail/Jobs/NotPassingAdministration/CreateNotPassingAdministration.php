<?php

namespace Modules\BulkEmail\Jobs\NotPassingAdministration;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use Modules\BulkEmail\Models\Schedule as NotPassingAdministration;
use Illuminate\Support\Facades\DB;

class CreateNotPassingAdministration extends Job implements HasOwner, HasSource, ShouldCreate
{
    public function handle(): NotPassingAdministration
    {
        //echo $this->request;
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model = NotPassingAdministration::create($this->request->all());
        });

        return $this->model;
    }
}