<?php

namespace Modules\BulkEmail\Jobs\Interview;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use Modules\BulkEmail\Models\Schedule as Interview;
use Illuminate\Support\Facades\DB;

class CreateInterview extends Job implements HasOwner, HasSource, ShouldCreate
{
    public function handle(): Interview
    {
        //echo $this->request;
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model = Interview::create($this->request->all());
        });

        return $this->model;
    }
}