<?php

namespace Modules\BulkEmail\Jobs\Interview;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldUpdate;
use Modules\BulkEmail\Models\Schedule as Interview;
use Illuminate\Support\Facades\DB;

class UpdateInterview extends Job implements HasOwner, ShouldUpdate
{
    public function handle(): Interview
    {
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model->update($value);
        });

        return $this->model;
    }
}