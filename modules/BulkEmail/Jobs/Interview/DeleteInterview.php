<?php

namespace Modules\BulkEmail\Jobs\Interview;

use App\Abstracts\Job;
use Modules\BulkEmail\Models\Schedule as Interview;

class DeleteInterview extends Job
{
    protected $interview;

    public function __construct(Interview $interview)
    {
        $this->interview = $interview;
    }

    public function handle(): bool
    {
        $this->authorize();
        $this->interview->delete();
        return true;
    }

    public function authorize()
    {
        if ($relationships = $this->getRelationships()) {
            $message = trans('messages.warning.deleted', ['name' => $this->interview->name, 'text' => implode(', ', $relationships)]);

            throw new \Exception($message);
        }
    }

    public function getRelationships(): array
    {
        $rels = [
            'members' => 'bulk-email::general.members',
        ];

        return $this->countRelationships($this->interview, $rels);
    }
}
