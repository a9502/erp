<?php

namespace Modules\BulkEmail\Jobs\Completenes;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldUpdate;
use Modules\BulkEmail\Models\Schedule as Completenes;
use Illuminate\Support\Facades\DB;

class UpdateCompletenes extends Job implements HasOwner, ShouldUpdate
{
    public function handle(): Completenes
    {
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model->update($value);
        });

        return $this->model;
    }
}