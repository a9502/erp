<?php

namespace Modules\BulkEmail\Jobs\Completenes;

use App\Abstracts\Job;
use Modules\BulkEmail\Models\Schedule as Completenes;

class DeleteCompletenes extends Job
{
    protected $completenes;

    public function __construct(Completenes $completenes)
    {
        $this->completenes = $completenes;
    }

    public function handle(): bool
    {
        $this->authorize();
        $this->completenes->delete();
        return true;
    }

    public function authorize()
    {
        if ($relationships = $this->getRelationships()) {
            $message = trans('messages.warning.deleted', ['name' => $this->completenes->name, 'text' => implode(', ', $relationships)]);

            throw new \Exception($message);
        }
    }

    public function getRelationships(): array
    {
        $rels = [
            'members' => 'bulk-email::general.members',
        ];

        return $this->countRelationships($this->completenes, $rels);
    }
}
