<?php

namespace Modules\BulkEmail\Jobs\Completenes;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use Modules\BulkEmail\Models\Schedule as Completenes;
use Illuminate\Support\Facades\DB;

class CreateCompletenes extends Job implements HasOwner, HasSource, ShouldCreate
{
    public function handle(): Completenes
    {
        //echo $this->request;
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model = Completenes::create($this->request->all());
        });

        return $this->model;
    }
}