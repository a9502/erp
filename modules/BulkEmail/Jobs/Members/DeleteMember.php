<?php

namespace Modules\BulkEmail\Jobs\Members;

use App\Abstracts\Job;
use Modules\BulkEmail\Models\Member as Model;

class DeleteMember extends Job
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function handle(): bool
    {
        $this->authorize();
        $this->model->delete();
        return true;
    }

    public function authorize()
    {
        return true;
    }
}
