<?php

namespace Modules\BulkEmail\Jobs\NotPassingTest;

use App\Abstracts\Job;
use Modules\BulkEmail\Models\Schedule as NotPassingTest;

class DeleteNotPassingTest extends Job
{
    protected $notPassingTest;

    public function __construct(NotPassingTest $notPassingTest)
    {
        $this->notPassingTest = $notPassingTest;
    }

    public function handle(): bool
    {
        $this->authorize();
        $this->notPassingTest->delete();
        return true;
    }

    public function authorize()
    {
        if ($relationships = $this->getRelationships()) {
            $message = trans('messages.warning.deleted', ['name' => $this->notPassingTest->name, 'text' => implode(', ', $relationships)]);

            throw new \Exception($message);
        }
    }

    public function getRelationships(): array
    {
        $rels = [
            'members' => 'bulk-email::general.members',
        ];

        return $this->countRelationships($this->notPassingTest, $rels);
    }
}
