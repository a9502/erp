<?php

namespace Modules\BulkEmail\Jobs\NotPassingTest;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldUpdate;
use Modules\BulkEmail\Models\Schedule as NotPassingTest;
use Illuminate\Support\Facades\DB;

class UpdateNotPassingTest extends Job implements HasOwner, ShouldUpdate
{
    public function handle(): NotPassingTest
    {
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model->update($value);
        });

        return $this->model;
    }
}