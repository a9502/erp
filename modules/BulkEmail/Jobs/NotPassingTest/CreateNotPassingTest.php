<?php

namespace Modules\BulkEmail\Jobs\NotPassingTest;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use Modules\BulkEmail\Models\Schedule as NotPassingTest;
use Illuminate\Support\Facades\DB;

class CreateNotPassingTest extends Job implements HasOwner, HasSource, ShouldCreate
{
    public function handle(): NotPassingTest
    {
        //echo $this->request;
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model = NotPassingTest::create($this->request->all());
        });

        return $this->model;
    }
}