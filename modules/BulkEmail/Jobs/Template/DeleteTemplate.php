<?php

namespace Modules\BulkEmail\Jobs\Template;

use App\Abstracts\Job;
use Modules\BulkEmail\Models\Template;
use Illuminate\Support\Facades\DB;
use App\Interfaces\Job\ShouldDelete;

class DeleteTemplate extends Job implements ShouldDelete
{
    public function handle(): bool
    {
        DB::transaction(function () {
            $this->model->delete();
        });

        return true;
    }
}
