<?php

namespace Modules\BulkEmail\Jobs\Template;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use Modules\BulkEmail\Models\Template;
use Illuminate\Support\Facades\DB;

class CreateTemplate extends Job implements HasOwner, HasSource, ShouldCreate
{
    public function handle(): Template
    {
        //echo $this->request;
        DB::transaction(function () {
            $value = $this->request->all();
            $value = array_merge($value,[
                'company_id'    => company_id()
            ]);
            $this->model = Template::create($this->request->all());
        });

        return $this->model;
    }
}