<?php

namespace Modules\BulkEmail\Jobs\Template;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldUpdate;
use Modules\BulkEmail\Models\Template;
use Illuminate\Support\Facades\DB;

class UpdateTemplate extends Job implements HasOwner, ShouldUpdate
{
    public function handle(): Template
    {
        DB::transaction(function () {
            $value = $this->request->all();
            $this->model->update($value);
        });

        return $this->model;
    }
}