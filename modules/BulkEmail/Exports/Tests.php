<?php

namespace Modules\BulkEmail\Exports;

use App\Abstracts\Export;
use Modules\BulkEmail\Models\BulkEmailTest as Model;

class Tests extends Export
{
    public function collection()
    {
        return Model::collectForExport($this->ids);
    }

    public function fields(): array
    {
        return [
            'name',
            'email',
            'schedule',
            'position',
            'username',
            'password'
        ];
    }
}
