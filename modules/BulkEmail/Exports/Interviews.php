<?php

namespace Modules\BulkEmail\Exports;

use App\Abstracts\Export;
use Modules\BulkEmail\Models\BulkEmailInterview as Model;

class Interviews extends Export
{
    public function collection()
    {
        return Model::collectForExport($this->ids);
    }

    public function fields(): array
    {
        return [
            'name',
            'email',
            'schedule',
            'start_time',
            'end_time',
            'zoom_link',
            'username',
            'password'
        ];
    }
}
