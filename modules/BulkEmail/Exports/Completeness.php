<?php

namespace Modules\BulkEmail\Exports;

use App\Abstracts\Export;
use Modules\BulkEmail\Models\Member as Model;

class Completeness extends Export
{
    public function collection()
    {
        return Model::collectForExport($this->ids);
    }

    public function fields(): array
    {
        return [
            'name',
            'email'
        ];
    }
}
