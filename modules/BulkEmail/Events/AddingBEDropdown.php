<?php

namespace Modules\BulkEmail\Events;

use App\Abstracts\Event;

class AddingBEDropdown extends Event
{
    public $show_dropdown;

    public function __construct(bool &$show_dropdown)
    {
        $this->show_dropdown = &$show_dropdown;
    }
}
