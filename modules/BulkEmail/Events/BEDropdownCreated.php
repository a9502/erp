<?php

namespace Modules\BulkEmail\Events;

use App\Abstracts\Event;

class BEDropdownCreated extends Event
{
    public $menu;

    public function __construct($menu)
    {
        $this->menu = $menu;
    }
}
