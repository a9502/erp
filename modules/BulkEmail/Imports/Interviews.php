<?php

namespace Modules\BulkEmail\Imports;

use App\Abstracts\Import;
use Modules\BulkEmail\Http\Requests\Interview as Request;
use Modules\BulkEmail\Models\Member as Model;

class Interviews extends Import
{
    public function model(array $row): Model
    {
        $scheduleId = request()->post('ref');
        $row = array_merge($row,['schedule_id' => $scheduleId]);
        return new Model($row);
    }

    public function rules(): array
    {
        return (new Request())->rules();
    }
}
