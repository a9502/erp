<?php

namespace Modules\BulkEmail\Imports;

use App\Abstracts\Import;
use Modules\BulkEmail\Http\Requests\Completenes as Request;
use Modules\BulkEmail\Models\Member as Model;

class Completeness extends Import
{
    public function model(array $row): Model
    {
        $scheduleId = request()->post('ref');
        $row = array_merge($row,['schedule_id' => $scheduleId,'schedule' => '','username' => '','password' => '']);
        return new Model($row);
    }

    public function rules(): array
    {
        return (new Request())->rules();
    }
}
