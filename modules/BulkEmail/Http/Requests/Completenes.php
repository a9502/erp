<?php

namespace Modules\BulkEmail\Http\Requests;

use App\Abstracts\Http\FormRequest;

class Completenes extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name'  => 'required|string',
            'email' => 'required|email'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
