<?php

namespace Modules\BulkEmail\Http\Requests;

use App\Abstracts\Http\FormRequest;

class Interview extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name'  => 'required|string',
            'email' => 'required|email',
            'schedule' => 'required|string',
            'start_time' => 'required|string',
            'end_time' => 'required|string',
            'zoom_link' => 'required|string',
            'username' => 'required|email',
            'password' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
