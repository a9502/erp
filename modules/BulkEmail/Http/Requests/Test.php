<?php

namespace Modules\BulkEmail\Http\Requests;

use App\Abstracts\Http\FormRequest;

class Test extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'name'  => 'required|string',
            'schedule' => 'required|string',
            'position' => 'required|string',
            'username' => 'required|email',
            'password' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
