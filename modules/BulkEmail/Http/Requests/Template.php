<?php

namespace Modules\BulkEmail\Http\Requests;

use App\Abstracts\Http\FormRequest;

class Template extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name'  => 'required|string|min:5',
            'subject' => 'required|string|min:5',
            'type' => 'required|string',
            'body' => 'required|string|min:10',
            'enabled' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
