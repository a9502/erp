<?php

namespace Modules\BulkEmail\Http\Controllers;

use App\Abstracts\Http\Controller;
use Modules\BulkEmail\Models\Template;
use Modules\BulkEmail\Http\Requests\Template as Request;
use Modules\BulkEmail\Jobs\Template\CreateTemplate;
use Modules\BulkEmail\Jobs\Template\UpdateTemplate;
use Modules\BulkEmail\Jobs\Template\DeleteTemplate;
use Illuminate\Http\JsonResponse;

class Templates extends Controller
{
    private $types = [
        'Test'      => 'Test',
        'Interview' => 'Interview',
        'Completenes' => 'Completenes',
        'Not Passing Administration Selection' => 'Not Passing Administration Selection',
        'Not Passing Online Test' => 'Not Passing Online Test'
    ];

    public function __construct()
    {
        $this->middleware('permission:create-bulk-email-templates')->only(['create', 'store', 'duplicate', 'import']);
        $this->middleware('permission:read-bulk-email-templates')->only(['index', 'show', 'edit', 'export']);
        $this->middleware('permission:update-bulk-email-templates')->only(['update', 'enable', 'disable']);
        $this->middleware('permission:delete-bulk-email-templates')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $templates = Template::collect();
        return view('bulk-email::templates.index', compact('templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $types = $this->types;
        return view('bulk-email::templates.create',compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $response = $this->ajaxDispatch(new CreateTemplate($request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.templates.index');
            $message = trans('messages.success.added', ['type' => trans_choice('bulk-email::general.template', 1)]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.templates.create');
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  Item  $item
     *
     * @return Response
     */
    public function duplicate(Template $template)
    {
        $clone = $template->duplicate();
        $message = trans('messages.success.duplicated', ['type' => trans_choice('bulk-email::general.templates', 1)]);
        flash($message)->success();
        return redirect()->route('bulk-email.templates.edit', $clone->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission  $permission
     *
     * @return Response
     */
    public function edit(Template $template)
    {
        $types = $this->types;
        return view('bulk-email::templates.edit', compact('template','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Template $template, Request $request)
    {
        $response = $this->ajaxDispatch(new UpdateTemplate($template, $request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.templates.index');
            $message = trans('messages.success.updated', ['type' => $template->name]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.templates.edit', $template->id);
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

     /**
     * Set disable the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function disable(Template $template): JsonResponse
    {
        $response = $this->ajaxDispatch(new UpdateTemplate($template, request()->merge(['enabled' => 0])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.disabled', ['type' => $template->name]);
        }

        return response()->json($response);
    }

     /**
     * Set enabled the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function enable(Template $template): JsonResponse
    {
        $response = $this->ajaxDispatch(new UpdateTemplate($template, request()->merge(['enabled' => 1])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.enabled', ['type' => $template->name]);
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function destroy(Template $template)
    {
        $response = $this->ajaxDispatch(new DeleteTemplate($template));

        $response['redirect'] = route('bulk-email.templates.index');

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $template->name]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}