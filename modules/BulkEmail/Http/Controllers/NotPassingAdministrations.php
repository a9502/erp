<?php

namespace Modules\BulkEmail\Http\Controllers;

use Modules\BulkEmail\Models\Member;
use Modules\BulkEmail\Models\Schedule as NotPassingAdministration;
use Modules\BulkEmail\Models\Template;

use Modules\BulkEmail\Jobs\NotPassingAdministration\CreateNotPassingAdministration;
use Modules\BulkEmail\Jobs\NotPassingAdministration\UpdateNotPassingAdministration;
use Modules\BulkEmail\Jobs\NotPassingAdministration\DeleteNotPassingAdministration;
use Modules\BulkEmail\Jobs\Members\DeleteMember;

use Modules\BulkEmail\Http\Requests\Schedule as Request;

use Modules\BulkEmail\Exports\NotPassingAdministrations as Export;
use Modules\BulkEmail\Imports\NotPassingAdministrations as Import;
use App\Http\Requests\Common\Import as ImportRequest;
use App\Abstracts\Http\Controller;
use Illuminate\Http\JsonResponse;
use App\Traits\Uploads;

class NotPassingAdministrations extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-bulk-email-tests')->only('create', 'store', 'duplicate', 'import');
        $this->middleware('permission:read-bulk-email-tests')->only('index', 'show', 'export');
        $this->middleware('permission:update-bulk-email-tests')->only('enable', 'disable');
        $this->middleware('permission:delete-bulk-email-tests')->only('destroy');
        $this->middleware('permission:read-bulk-email-tests')->only('edit');
        $this->middleware('permission:update-bulk-email-tests')->only('update');
    }

    //use Uploads;

    

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = NotPassingAdministration::notPassingAdministration()->collect(['schedule_at' => 'desc']);
        return view('bulk-email::not-passing-administration.index', compact('items'));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show(NotPassingAdministration $items)
    {
        return view('bulk-email::not-passing-administration.show', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $templates = Template::enabled()->notPassingAdministration()->pluck('name', 'id');
        return view('bulk-email::not-passing-administration.create',compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $response = $this->ajaxDispatch(new CreateNotPassingAdministration($request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.not-passing-administration.edit',$response['data']['id']).'?next=member';
            $message = trans('messages.success.added', ['type' => trans_choice('bulk-email::general.not_passing_administration', 1)]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.not-passing-administration.create');
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  Item  $item
     *
     * @return Response
     */
    public function duplicate(NotPassingAdministration $item)
    {
        $clone = $item->duplicate();
        $id = $clone->id;
        $update = $clone->update(['status' => NotPassingAdministration::DRAFT,'enabled' => true]);
        
        //clone all member 
        $members = Member::where('schedule_id',$item->id)->get();
        if($members) 
        {
            foreach($members as $member) 
            {
                $clone = $member->duplicate();
                $clone->update(['schedule_id' => $id]);
            }
        }

        $message = trans('messages.success.duplicated', ['type' => trans_choice('bulk-email::general.not-passing-administration', 1)]);
        flash($message)->success();
        return redirect()->route('bulk-email.not-passing-administration.edit', [$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission  $permission
     *
     * @return Response
     */
    public function edit(NotPassingAdministration $notPassingAdministration)
    {
        if($notPassingAdministration->status != NotPassingAdministration::DRAFT) 
            abort(404);

        if(request()->get('next') == 'member') 
        {
            $members = Member::where('schedule_id',$notPassingAdministration->id)->collect();
            return view('bulk-email::not-passing-administration.member.index',compact('members','notPassingAdministration'));
        } 

        $templates = Template::enabled()->notPassingAdministration()->pluck('name', 'id');
        return view('bulk-email::not-passing-administration.edit',compact('notPassingAdministration','templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function update(NotPassingAdministration $notPassingAdministration, Request $request)
    {
        if($notPassingAdministration->status != NotPassingAdministration::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateNotPassingAdministration($notPassingAdministration, $request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.not-passing-administration.edit', $notPassingAdministration->id).'?next=member';
            $message = trans('messages.success.updated', ['type' => $notPassingAdministration->name]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.not-passing-administration.edit', $notPassingAdministration->id);
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Export the index for viewing the specified resource.
     *
     * @return Response
     */
    public function export()
    {
        return $this->exportExcel(new Export, trans_choice('bulk-email::general.not-passing-administration', 2));
    }

    /**
     * Import the index for viewing the specified resource.
     *
     * @return Response
     */
    public function import(ImportRequest $request)
    {
        $response = $this->importExcel(new Import, $request, trans_choice('bulk-email::general.not-passing-administration', 2));

        if ($response['success']) {
            $id = $request->post('ref');
            $totalMember = Member::where('schedule_id',$id)->count();
            NotPassingAdministration::where('id',$id)->update(['total_member' => $totalMember]);

            $response['redirect'] = route('bulk-email.not-passing-administration.edit', $id).'?next=member';
            flash($response['message'])->success();
        } else {
            $response['redirect'] = route('bulk-email.not-passing-administration.index');
            flash($response['message'])->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Set disable the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function disable(NotPassingAdministration $item): JsonResponse
    {
        if($item->status != NotPassingAdministration::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateNotPassingAdministration($item, request()->merge(['enabled' => 0])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.disabled', ['type' => $item->name]);
        }

        return response()->json($response);
    }

    /**
     * Set enabled the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function enable(NotPassingAdministration $item): JsonResponse
    {
        if($item->status != NotPassingAdministration::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateNotPassingAdministration($item, request()->merge(['enabled' => 1])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.enabled', ['type' => $item->schedule_at]);
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function destroy(NotPassingAdministration $notPassingAdministration)
    {
        if($notPassingAdministration->status != NotPassingAdministration::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new DeleteNotPassingAdministration($notPassingAdministration));

        $response['redirect'] = route('bulk-email.not-passing-administration.index');

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $notPassingAdministration->schedule_at]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified member resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function memberDestroy(Member $member)
    {
        $id = $member->schedule_id;
        $response = $this->ajaxDispatch(new DeleteMember($member));
        $response['redirect'] = route('bulk-email.not-passing-administration.edit', $id).'?next=member';

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $member->name]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}