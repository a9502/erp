<?php

namespace Modules\BulkEmail\Http\Controllers;

use Modules\BulkEmail\Models\Member;
use Modules\BulkEmail\Models\Schedule as Interview;
use Modules\BulkEmail\Models\Template;

use Modules\BulkEmail\Jobs\Interview\CreateInterview;
use Modules\BulkEmail\Jobs\Interview\UpdateInterview;
use Modules\BulkEmail\Jobs\Interview\DeleteInterview;
use Modules\BulkEmail\Jobs\Members\DeleteMember;

use Modules\BulkEmail\Http\Requests\Schedule as Request;

use Modules\BulkEmail\Exports\Interviews as Export;
use Modules\BulkEmail\Imports\Interviews as Import;

use App\Http\Requests\Common\Import as ImportRequest;
use App\Abstracts\Http\Controller;
use Illuminate\Http\JsonResponse;
use App\Traits\Uploads;

class Interviews extends Controller
{
    use Uploads;

    public function __construct()
    {
        $this->middleware('permission:create-bulk-email-interviews')->only(['create', 'store', 'duplicate', 'import']);
        $this->middleware('permission:read-bulk-email-interviews')->only(['index', 'show', 'edit', 'export']);
        $this->middleware('permission:update-bulk-email-interviews')->only(['update', 'enable', 'disable']);
        $this->middleware('permission:delete-bulk-email-interviews')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $interviews = Interview::interview()->collect();
        return view('bulk-email::interviews.index', compact('interviews'));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show(Interview $interview)
    {
        return view('bulk-email::interviews.show', compact('interview'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $templates = Template::enabled()->interview()->pluck('name', 'id');
        return view('bulk-email::interviews.create',compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $response = $this->ajaxDispatch(new CreateInterview($request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.interviews.edit',$response['data']['id']).'?next=member';
            $message = trans('messages.success.added', ['type' => trans_choice('bulk-email::general.interviews', 1)]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.interviews.create');
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  Item  $item
     *
     * @return Response
     */
    public function duplicate(Interview $interview)
    {
        $clone = $interview->duplicate();
        $id = $clone->id;
        $update = $clone->update(['status' => Interview::DRAFT,'enabled' => true]);

        //clone all member 
        $members = Member::where('schedule_id',$interview->id)->get();
        if($members) 
        {
            foreach($members as $member) 
            {
                $clone = $member->duplicate();
                $clone->update(['schedule_id' => $id]);
            }
        }

        $message = trans('messages.success.duplicated', ['type' => trans_choice('bulk-email::general.interviews', 1)]);
        flash($message)->success();
        return redirect()->route('bulk-email.interviews.edit', [$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission  $permission
     *
     * @return Response
     */
    public function edit(Interview $interview)
    {
        if($interview->status != Interview::DRAFT) 
            abort(404);

        if(request()->get('next') == 'member') 
        {
            $members = Member::where('schedule_id',$interview->id)->collect();
            return view('bulk-email::interviews.member.index',compact('members','interview'));
        } 

        $templates = Template::enabled()->interview()->pluck('name', 'id');
        return view('bulk-email::interviews.edit',compact('interview','templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Interview $interview, Request $request)
    {
        if($interview->status != Interview::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateInterview($interview, $request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.interviews.edit', $interview->id).'?next=member';
            $message = trans('messages.success.updated', ['type' => $interview->name]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.interviews.edit', $interview->id);
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Export the index for viewing the specified resource.
     *
     * @return Response
     */
    public function export()
    {
        return $this->exportExcel(new Export, trans_choice('bulk-email::general.interviews', 2));
    }

    /**
     * Import the index for viewing the specified resource.
     *
     * @return Response
     */
    public function import(ImportRequest $request)
    {
        $response = $this->importExcel(new Import, $request, trans_choice('bulk-email::general.interviews', 2));
        $id = $request->post('ref');

        if ($response['success']) {
            
            $totalMember = Member::where('schedule_id',$id)->count();
            Interview::where('id',$id)->update(['total_member' => $totalMember]);

            $response['redirect'] = route('bulk-email.interviews.edit', $id).'?next=member';
            flash($response['message'])->success();
        } else {
            $response['redirect'] = route('bulk-email.interviews.edit', $id).'?next=member';
            flash($response['message'])->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Set disable the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function disable(Interview $interview): JsonResponse
    {
        if($interview->status != Interview::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateInterview($interview, request()->merge(['enabled' => 0])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.disabled', ['type' => $interview->name]);
        }

        return response()->json($response);
    }

    /**
     * Set enabled the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function enable(Interview $interview): JsonResponse
    {
        if($interview->status != Interview::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateInterview($interview, request()->merge(['enabled' => 1])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.enabled', ['type' => $interview->schedule_at]);
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function destroy(Interview $interview)
    {
        if($interview->status != Interview::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new DeleteInterview($interview));

        $response['redirect'] = route('bulk-email.interviews.index');

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $interview->schedule_at]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified member resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function memberDestroy(Member $member)
    {
        $id = $member->schedule_id;
        $response = $this->ajaxDispatch(new DeleteMember($member));
        $response['redirect'] = route('bulk-email.interviews.edit', $id).'?next=member';

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $member->name]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}