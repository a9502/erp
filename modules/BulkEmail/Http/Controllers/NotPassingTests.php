<?php

namespace Modules\BulkEmail\Http\Controllers;

use Modules\BulkEmail\Models\Member;
use Modules\BulkEmail\Models\Schedule as NotPassingTest;
use Modules\BulkEmail\Models\Template;

use Modules\BulkEmail\Jobs\NotPassingTest\CreateNotPassingTest;
use Modules\BulkEmail\Jobs\NotPassingTest\UpdateNotPassingTest;
use Modules\BulkEmail\Jobs\NotPassingTest\DeleteNotPassingTest;
use Modules\BulkEmail\Jobs\Members\DeleteMember;

use Modules\BulkEmail\Http\Requests\Schedule as Request;

use Modules\BulkEmail\Exports\NotPassingTests as Export;
use Modules\BulkEmail\Imports\NotPassingTests as Import;
use App\Http\Requests\Common\Import as ImportRequest;
use App\Abstracts\Http\Controller;
use Illuminate\Http\JsonResponse;
use App\Traits\Uploads;

class NotPassingTests extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-bulk-email-tests')->only('create', 'store', 'duplicate', 'import');
        $this->middleware('permission:read-bulk-email-tests')->only('index', 'show', 'export');
        $this->middleware('permission:update-bulk-email-tests')->only('enable', 'disable');
        $this->middleware('permission:delete-bulk-email-tests')->only('destroy');
        $this->middleware('permission:read-bulk-email-tests')->only('edit');
        $this->middleware('permission:update-bulk-email-tests')->only('update');
    }

    //use Uploads;

    

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = NotPassingTest::notPassingTest()->collect(['schedule_at' => 'desc']);
        return view('bulk-email::not-passing-test.index', compact('items'));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show(NotPassingTest $items)
    {
        return view('bulk-email::not-passing-test.show', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $templates = Template::enabled()->notPassingTest()->pluck('name', 'id');
        return view('bulk-email::not-passing-test.create',compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $response = $this->ajaxDispatch(new CreateNotPassingTest($request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.not-passing-test.edit',$response['data']['id']).'?next=member';
            $message = trans('messages.success.added', ['type' => trans_choice('bulk-email::general.not_passing_test', 1)]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.not-passing-test.create');
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  Item  $item
     *
     * @return Response
     */
    public function duplicate(NotPassingTest $item)
    {
        $clone = $item->duplicate();
        $id = $clone->id;
        $update = $clone->update(['status' => NotPassingTest::DRAFT,'enabled' => true]);
        
        //clone all member 
        $members = Member::where('schedule_id',$item->id)->get();
        if($members) 
        {
            foreach($members as $member) 
            {
                $clone = $member->duplicate();
                $clone->update(['schedule_id' => $id]);
            }
        }

        $message = trans('messages.success.duplicated', ['type' => trans_choice('bulk-email::general.not-passing-test', 1)]);
        flash($message)->success();
        return redirect()->route('bulk-email.not-passing-test.edit', [$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission  $permission
     *
     * @return Response
     */
    public function edit(NotPassingTest $notPassingTest)
    {
        if($notPassingTest->status != NotPassingTest::DRAFT) 
            abort(404);

        if(request()->get('next') == 'member') 
        {
            $members = Member::where('schedule_id',$notPassingTest->id)->collect();
            return view('bulk-email::not-passing-test.member.index',compact('members','notPassingTest'));
        } 

        $templates = Template::enabled()->notPassingTest()->pluck('name', 'id');
        return view('bulk-email::not-passing-test.edit',compact('notPassingTest','templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function update(NotPassingTest $notPassingTest, Request $request)
    {
        if($notPassingTest->status != NotPassingTest::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateNotPassingTest($notPassingTest, $request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.not-passing-test.edit', $notPassingTest->id).'?next=member';
            $message = trans('messages.success.updated', ['type' => $notPassingTest->name]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.not-passing-test.edit', $notPassingTest->id);
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Export the index for viewing the specified resource.
     *
     * @return Response
     */
    public function export()
    {
        return $this->exportExcel(new Export, trans_choice('bulk-email::general.not-passing-test', 2));
    }

    /**
     * Import the index for viewing the specified resource.
     *
     * @return Response
     */
    public function import(ImportRequest $request)
    {
        $response = $this->importExcel(new Import, $request, trans_choice('bulk-email::general.not-passing-test', 2));

        if ($response['success']) {
            $id = $request->post('ref');
            $totalMember = Member::where('schedule_id',$id)->count();
            NotPassingTest::where('id',$id)->update(['total_member' => $totalMember]);

            $response['redirect'] = route('bulk-email.not-passing-test.edit', $id).'?next=member';
            flash($response['message'])->success();
        } else {
            $response['redirect'] = route('bulk-email.not-passing-test.index');
            flash($response['message'])->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Set disable the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function disable(NotPassingTest $item): JsonResponse
    {
        if($item->status != NotPassingTest::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateNotPassingTest($item, request()->merge(['enabled' => 0])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.disabled', ['type' => $item->name]);
        }

        return response()->json($response);
    }

    /**
     * Set enabled the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function enable(NotPassingTest $item): JsonResponse
    {
        if($item->status != NotPassingTest::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateNotPassingTest($item, request()->merge(['enabled' => 1])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.enabled', ['type' => $item->schedule_at]);
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function destroy(NotPassingTest $notPassingTest)
    {
        if($notPassingTest->status != NotPassingTest::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new DeleteNotPassingTest($notPassingTest));

        $response['redirect'] = route('bulk-email.not-passing-test.index');

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $notPassingTest->schedule_at]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified member resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function memberDestroy(Member $member)
    {
        $id = $member->schedule_id;
        $response = $this->ajaxDispatch(new DeleteMember($member));
        $response['redirect'] = route('bulk-email.not-passing-test.edit', $id).'?next=member';

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $member->name]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}