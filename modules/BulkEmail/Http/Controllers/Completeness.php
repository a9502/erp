<?php

namespace Modules\BulkEmail\Http\Controllers;

use Modules\BulkEmail\Models\Member;
use Modules\BulkEmail\Models\Schedule as Completenes;
use Modules\BulkEmail\Models\Template;

use Modules\BulkEmail\Jobs\Completenes\CreateCompletenes;
use Modules\BulkEmail\Jobs\Completenes\UpdateCompletenes;
use Modules\BulkEmail\Jobs\Completenes\DeleteCompletenes;
use Modules\BulkEmail\Jobs\Members\DeleteMember;

use Modules\BulkEmail\Http\Requests\Schedule as Request;

use Modules\BulkEmail\Exports\Completeness as Export;
use Modules\BulkEmail\Imports\Completeness as Import;
use App\Http\Requests\Common\Import as ImportRequest;
use App\Abstracts\Http\Controller;
use Illuminate\Http\JsonResponse;
use App\Traits\Uploads;

class Completeness extends Controller
{
    private $types = [
        'Test'      => 'Test',
        'Interview' => 'Interview',
        'Completeness' => 'Completeness'
    ];

    public function __construct()
    {
        $this->middleware('permission:create-bulk-email-tests')->only('create', 'store', 'duplicate', 'import');
        $this->middleware('permission:read-bulk-email-tests')->only('index', 'show', 'export');
        $this->middleware('permission:update-bulk-email-tests')->only('enable', 'disable');
        $this->middleware('permission:delete-bulk-email-tests')->only('destroy');
        $this->middleware('permission:read-bulk-email-tests')->only('edit');
        $this->middleware('permission:update-bulk-email-tests')->only('update');
    }

    //use Uploads;

    

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $completeness = Completenes::completenes()->collect(['schedule_at' => 'desc']);
        return view('bulk-email::completeness.index', compact('completeness'));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show(Completenes $completenes)
    {
        return view('bulk-email::completeness.show', compact('completenes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $templates = Template::enabled()->completenes()->pluck('name', 'id');
        return view('bulk-email::completeness.create',compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $response = $this->ajaxDispatch(new CreateCompletenes($request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.completeness.edit',$response['data']['id']).'?next=member';
            $message = trans('messages.success.added', ['type' => trans_choice('bulk-email::general.Completeness', 1)]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.completeness.create');
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  Item  $item
     *
     * @return Response
     */
    public function duplicate(Completenes $completenes)
    {
        $clone = $completenes->duplicate();
        $id = $clone->id;
        $update = $clone->update(['status' => Completenes::DRAFT,'enabled' => true]);
        
        //clone all member 
        $members = Member::where('schedule_id',$completenes->id)->get();
        if($members) 
        {
            foreach($members as $member) 
            {
                $clone = $member->duplicate();
                $clone->update(['schedule_id' => $id]);
            }
        }

        $message = trans('messages.success.duplicated', ['type' => trans_choice('bulk-email::general.completeness', 1)]);
        flash($message)->success();
        //return redirect()->route('bulk-email.Completeness.edit', [$id,'next' => 'member']);
        return redirect()->route('bulk-email.completeness.edit', [$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission  $permission
     *
     * @return Response
     */
    public function edit(Completenes $completeness)
    {
        if($completeness->status != Completenes::DRAFT) 
            abort(404);

        if(request()->get('next') == 'member') 
        {
            $members = Member::where('schedule_id',$completeness->id)->collect();
            return view('bulk-email::completeness.member.index',compact('members','completeness'));
        } 

        $templates = Template::enabled()->completenes()->pluck('name', 'id');
        return view('bulk-email::completeness.edit',compact('completeness','templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Completenes $completeness, Request $request)
    {
        if($completeness->status != Completenes::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateCompletenes($completeness, $request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.completeness.edit', $completeness->id).'?next=member';
            $message = trans('messages.success.updated', ['type' => $completeness->name]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.completeness.edit', $completeness->id);
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Export the index for viewing the specified resource.
     *
     * @return Response
     */
    public function export()
    {
        return $this->exportExcel(new Export, trans_choice('bulk-email::general.completeness', 2));
    }

    /**
     * Import the index for viewing the specified resource.
     *
     * @return Response
     */
    public function import(ImportRequest $request)
    {
        $response = $this->importExcel(new Import, $request, trans_choice('bulk-email::general.completeness', 2));

        if ($response['success']) {
            $id = $request->post('ref');
            $totalMember = Member::where('schedule_id',$id)->count();
            Completenes::where('id',$id)->update(['total_member' => $totalMember]);

            $response['redirect'] = route('bulk-email.completeness.edit', $id).'?next=member';
            flash($response['message'])->success();
        } else {
            $response['redirect'] = route('bulk-email.completeness.index');
            flash($response['message'])->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Set disable the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function disable(Completenes $completenes): JsonResponse
    {
        if($completenes->status != Completenes::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateCompletenes($completenes, request()->merge(['enabled' => 0])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.disabled', ['type' => $completenes->name]);
        }

        return response()->json($response);
    }

     /**
     * Set enabled the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function enable(Completenes $completenes): JsonResponse
    {
        if($completenes->status != Completenes::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateCompletenes($completenes, request()->merge(['enabled' => 1])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.enabled', ['type' => $completenes->schedule_at]);
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function destroy(Completenes $completenes)
    {
        if($completenes->status != Completenes::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new DeleteCompletenes($completenes));

        $response['redirect'] = route('bulk-email.completeness.index');

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $completenes->schedule_at]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified member resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function memberDestroy(Member $member)
    {
        $id = $member->schedule_id;
        $response = $this->ajaxDispatch(new DeleteMember($member));
        $response['redirect'] = route('bulk-email.completeness.edit', $id).'?next=member';

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $member->name]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}