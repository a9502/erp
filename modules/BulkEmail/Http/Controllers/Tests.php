<?php

namespace Modules\BulkEmail\Http\Controllers;

use Modules\BulkEmail\Models\Member;
use Modules\BulkEmail\Models\Schedule as Test;
use Modules\BulkEmail\Models\Template;

use Modules\BulkEmail\Jobs\Test\CreateTest;
use Modules\BulkEmail\Jobs\Test\UpdateTest;
use Modules\BulkEmail\Jobs\Test\DeleteTest;
use Modules\BulkEmail\Jobs\Members\DeleteMember;

use Modules\BulkEmail\Http\Requests\Schedule as Request;

use Modules\BulkEmail\Exports\Tests as Export;
use Modules\BulkEmail\Imports\Tests as Import;
use App\Http\Requests\Common\Import as ImportRequest;
use App\Abstracts\Http\Controller;
use Illuminate\Http\JsonResponse;
use App\Traits\Uploads;

class Tests extends Controller
{
    //use Uploads;

    private $types = [
        'Test'      => 'Test',
        'Interview' => 'Interview'
    ];

    public function __construct()
    {
        $this->middleware('permission:create-bulk-email-tests')->only(['create', 'store', 'duplicate', 'import']);
        $this->middleware('permission:read-bulk-email-tests')->only(['index', 'show', 'edit', 'export']);
        $this->middleware('permission:update-bulk-email-tests')->only(['update', 'enable', 'disable']);
        $this->middleware('permission:delete-bulk-email-tests')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tests = Test::test()->collect(['schedule_at' => 'desc']);
        return view('bulk-email::tests.index', compact('tests'));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show(Test $test)
    {
        return view('bulk-email::tests.show', compact('test'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $templates = Template::enabled()->test()->pluck('name', 'id');
        return view('bulk-email::tests.create',compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $response = $this->ajaxDispatch(new CreateTest($request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.tests.edit',$response['data']['id']).'?next=member';
            $message = trans('messages.success.added', ['type' => trans_choice('bulk-email::general.tests', 1)]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.tests.create');
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  Item  $item
     *
     * @return Response
     */
    public function duplicate(Test $test)
    {
        $clone = $test->duplicate();
        $id = $clone->id;
        $update = $clone->update(['status' => Test::DRAFT,'enabled' => true]);
        
        //clone all member 
        $members = Member::where('schedule_id',$test->id)->get();
        if($members) 
        {
            foreach($members as $member) 
            {
                $clone = $member->duplicate();
                $clone->update(['schedule_id' => $id]);
            }
        }

        $message = trans('messages.success.duplicated', ['type' => trans_choice('bulk-email::general.tests', 1)]);
        flash($message)->success();
        //return redirect()->route('bulk-email.tests.edit', [$id,'next' => 'member']);
        return redirect()->route('bulk-email.tests.edit', [$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission  $permission
     *
     * @return Response
     */
    public function edit(Test $test)
    {
        if($test->status != Test::DRAFT) 
            abort(404);

        if(request()->get('next') == 'member') 
        {
            $members = Member::where('schedule_id',$test->id)->collect();
            return view('bulk-email::tests.member.index',compact('members','test'));
        } 

        $templates = Template::enabled()->test()->pluck('name', 'id');
        return view('bulk-email::tests.edit',compact('test','templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Test $test, Request $request)
    {
        if($test->status != Test::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateTest($test, $request));

        if ($response['success']) {
            $response['redirect'] = route('bulk-email.tests.edit', $test->id).'?next=member';
            $message = trans('messages.success.updated', ['type' => $test->name]);
            flash($message)->success();
        } else {
            $response['redirect'] = route('bulk-email.tests.edit', $test->id);
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Export the index for viewing the specified resource.
     *
     * @return Response
     */
    public function export()
    {
        return $this->exportExcel(new Export, trans_choice('bulk-email::general.tests', 2));
    }

    /**
     * Import the index for viewing the specified resource.
     *
     * @return Response
     */
    public function import(ImportRequest $request)
    {
        $response = $this->importExcel(new Import, $request, trans_choice('bulk-email::general.online_tests', 2));

        if ($response['success']) {
            $id = $request->post('ref');
            $totalMember = Member::where('schedule_id',$id)->count();
            Test::where('id',$id)->update(['total_member' => $totalMember]);

            $response['redirect'] = route('bulk-email.tests.edit', $id).'?next=member';
            flash($response['message'])->success();
        } else {
            $response['redirect'] = route('bulk-email.tests.edit', $id).'?next=member';
            flash($response['message'])->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Set disable the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function disable(Test $test): JsonResponse
    {
        if($test->status != Test::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateTest($test, request()->merge(['enabled' => 0])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.disabled', ['type' => $test->name]);
        }

        return response()->json($response);
    }

     /**
     * Set enabled the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     *
     * @return Response
     */
    public function enable(Test $test): JsonResponse
    {
        if($test->status != Test::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new UpdateTest($test, request()->merge(['enabled' => 1])));

        if ($response['success']) {
            $response['message'] = trans('messages.success.enabled', ['type' => $test->schedule_at]);
        }

        return response()->json($response);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function destroy(Test $test)
    {
        if($test->status != Test::DRAFT) 
            abort(404);

        $response = $this->ajaxDispatch(new DeleteTest($test));

        $response['redirect'] = route('bulk-email.tests.index');

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $test->schedule_at]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified member resource from storage.
     *
     * @param  Permission $permission
     *
     * @return Response
     */
    public function memberDestroy(Member $member)
    {
        $id = $member->schedule_id;
        $response = $this->ajaxDispatch(new DeleteMember($member));
        $response['redirect'] = route('bulk-email.tests.edit', $id).'?next=member';

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $member->name]);
            flash($message)->success();
        } else {
            $message = $response['message'];
            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}