<?php

return [
    'body' => 'Body',
    'bulk_email'=> 'Bulk Email',
    'name' => 'BulkEmail',
    'completeness' => 'Complete|Completeness',
    'description' => 'This is my awesome module',
    'draft' => 'Draft',
    'interviews' => 'Online interviews',
    'logs' => 'Mail Logs',
    'members' => 'Member|Members',
    'online_interviews' => 'Online Interview|Online Interviews',
    'online_tests' => 'Online Test|Online Tests',
    'position' => 'Position',
    'process' => 'Process',
    'schedule' => 'Schedule',
    'schedule_at' => 'Schedule at',
    'status' => 'Status',
    'subject' => 'Subject',
    'templates' => 'Template|Templates',
    'tests' => 'Online Tests',
    'total_member' => 'Total Member',
    'type' => 'Type',
    'zoom' => 'Zoom'

];