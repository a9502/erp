@php 
use Modules\BulkEMail\Models\Schedule;
@endphp

@extends('layouts.admin')

@section('title', trans_choice('bulk-email::general.online_interviews',2))

@section('new_button')
    <span>
        <a href="{{ route('bulk-email.interviews.create') }}" class="btn btn-success btn-sm">{{ trans('general.add_new') }}</a>
    </span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header border-bottom-0" :class="[{'bg-gradient-primary': bulk_action.show}]">
            {!! Form::open([
                'method' => 'GET',
                'url' => 'bulk-email.interviews.index',
                'role' => 'form',
                'class' => 'mb-0'
            ]) !!}
                <div class="align-items-center" v-if="!bulk_action.show">
                    <x-search-string model="Modules\BulkEmail\Models\Schedule" />
                </div>
                {{ Form::bulkActionRowGroup('bulk-email::general.interviews', $bulk_actions, ['group' => 'bulk-email', 'type' => 'interviews']) }}
            {!! Form::close() !!}
        </div>

        <div class="table-responsive">
            <table class="table table-flush table-hover">
                <thead class="thead-light">
                    <tr class="row table-head-line">
                        <th class="col-md-1 col-lg-1 col-xl-1 hidden-sm">{{ Form::bulkActionAllGroup() }}</th>
                        <th class="col-md-3">@sortablelink('name', trans('general.name'))</th>
                        <th class="col-md-2">@sortablelink('schedule_at', trans('bulk-email::general.schedule_at'))</th>
                        <th class="col-md-1 hidden-xs text-center">@sortablelink('total_member',trans('bulk-email::general.total_member'))</th>
                        <th class="col-md-2 hidden-xs">@sortablelink('enabled', trans('general.enabled'))</th>
                        <th class="col-md-2 hidden-xs">@sortablelink('status', trans_choice('bulk-email::general.status', 1))</th>
                        <th class="col-md-1">{{ trans('general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($interviews as $item)
                        <tr class="row  border-top-1">
                            <td class="col-sm-2 col-md-1 col-lg-1 col-xl-1 hidden-sm border-0">
                                {{ Form::bulkActionGroup($item->id, $item->name) }}
                            </td>
                            <td class="col-md-3 border-0">
                                <a href="{{ route('bulk-email.interviews.edit', $item->id) }}">{{ $item->name }}</a>
                            </td>
                            <td class="col-md-2 border-0">
                                <a href="{{ route('bulk-email.interviews.edit', $item->id) }}">{{ $item->schedule_at }}</a>
                            </td>
                            <td class="col-md-1 border-0 hidden-xs text-center">
                                {{ number_format($item->total_member,0) }}
                            </td>
                            <td class="col-md-2 border-0 hidden-xs">
                                @if($item->status == Schedule::DRAFT)

                                    {{ Form::enabledGroup($item->id, $item->name, $item->enabled) }}

                                @else
                                    <badge rounded type="danger" class="mw-60">{{ trans('general.no') }}</badge>

                                @endif
                            </td>
                            <td class="col-md-2 border-0 hidden-xs text-nowrap">
                                @if ($item->status == Schedule::DRAFT)
                                    <badge rounded type="primary" class="mw-60">{{ trans('bulk-email::general.draft') }}</badge>
                                @elseif ($item->status == Schedule::PROCESS)
                                    <badge rounded type="danger" class="mw-60">{{ trans('bulk-email::general.process') }}</badge>
                                @else
                                    <badge rounded type="success" class="mw-60">{{ trans('bulk-email::general.finish') }}</badge>
                                @endif
                            </td>
                            <td class="col-md-1 border-0 hidden-xs text-nowrap">
                                <div class="dropdown">
                                    <a class="btn btn-neutral btn-sm text-light items-align-center p-2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h text-muted"></i>
                                    </a>
                                    
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="{{ route('bulk-email.interviews.duplicate', $item->id) }}">{{ trans('general.duplicate') }}</a>
                                        @if($item->status == Schedule::DRAFT)
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="{{ route('bulk-email.interviews.edit', $item->id) }}">{{ trans('general.edit') }}</a>
                                            
                                            <div class="dropdown-divider"></div>
                                            {!! Form::deleteLink($item,'bulk-email.interviews.destroy') !!}
                                        @endif
                                        
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer table-action">
            <div class="row align-items-center">
                @include('partials.admin.pagination', ['items' => $interviews])
            </div>
        </div>

    </div>
@endsection

@push('scripts_start')
    <script src="{{ asset('js/modules/bulk-email/app.js?v=' . module_version('bulk-email')) }}"></script>
@endpush

