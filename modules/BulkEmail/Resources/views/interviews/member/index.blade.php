@extends('layouts.admin')

@section('title', trans_choice('bulk-email::general.members',2))

@section('new_button')
    <span>
        <a href="{{ route('import.create', ['bulk-email','interviews','ref' => $interview->id ]) }}" class="btn btn-white btn-sm">{{ trans('import.import') }}</a>
    </span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header border-bottom-0" :class="[{'bg-gradient-primary': bulk_action.show}]">
            <ul id="progressbar">
                <li id="account"><strong>{{ __('bulk-email::general.schedule') }}</strong></li>
                <li class="active" id="personal"><strong>{{ trans_choice('bulk-email::general.members',2) }}</strong></li>
            </ul>

            {!! Form::open([
                'method' => 'GET',
                'url' => 'bulk-email.interviews.index',
                'role' => 'form',
                'class' => 'mb-0'
            ]) !!}
                <div class="align-items-center" v-if="!bulk_action.show">
                    <x-search-string model="Modules\BulkEmail\Models\Member" />
                </div>
                {{ Form::bulkActionRowGroup('bulk-email::general.interviews', $bulk_actions, ['group' => 'bulk-email', 'type' => 'interviews']) }}
            {!! Form::close() !!}
        </div>

        <div class="table-responsive">
            <table class="table table-flush table-hover">
                <thead class="thead-light">
                    <tr class="row table-head-line">
                        <th class="col-md-1">{{ Form::bulkActionAllGroup() }}</th>
                        <th class="col-md-3">@sortablelink('name', trans('general.name'))</th>
                        <th class="col-md-3">@sortablelink('email',trans('general.email'))</th>
                        <th class="col-md-2">@sortablelink('schedule', trans('bulk-email::general.schedule'))</th>
                        <th class="col-md-2 hidden-xs">@sortablelink('zoom_link', trans_choice('bulk-email::general.zoom', 1))</th>
                        <th class="col-md-1">{{ trans('general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($members as $item)
                        <tr class="row  border-top-1">
                            <td class="col-sm-2 col-md-1 col-lg-1 col-xl-1 hidden-sm border-0">
                                {{ Form::bulkActionGroup($item->id, $item->name) }}
                            </td>
                            <td class="col-md-3 border-0">
                                <a href="{{ route('employees.employees.show', $item->id) }}">{{ $item->name }}</a>
                            </td>
                            <td class="col-md-3 border-0">
                                {{ $item->email }}
                            </td>
                            <td class="col-md-2 border-0">
                                {{ $item->schedule }} {{ $item->start_time }} - {{ $item->end_time }}
                            </td>
                            <td class="col-md-2 border-0">
                                {{ $item->zoom_link }}
                            </td>
                            <td class="col-md-1 border-0">
                                <div class="dropdown">
                                    <a class="btn btn-neutral btn-sm text-light items-align-center p-2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h text-muted"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        {!! Form::deleteLink($item,'bulk-email.interviews.members.destroy') !!}
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer table-action">
            <div class="row align-items-center">
                @include('partials.admin.pagination', ['items' => $members])
            </div>
            <div class="row mt-2">
                <div class="col-md-12 text-right">
                    <a href="{{ route('bulk-email.interviews.edit', [$interview->id]) }}" class="btn btn-white btn-md">{{ trans('general.back') }}</a>
                    <a href="{{ route('bulk-email.interviews.index') }}" class="btn btn-success btn-md">{{ trans('general.finish') }}</a>
                </div>
            </span>
        </div>

    </div>
@endsection

@push('scripts_start')
    <script src="{{ asset('js/modules/bulk-email/app.js?v=' . module_version('bulk-email')) }}"></script>
@endpush

@push('css')
<style>
#progressbar {
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey
}
#progressbar .active {
    color: #673AB7
}
#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 50%;
    float: left;
    position: relative;
    font-weight: 400;
    text-align: center
}

#progressbar #account:before {
    font-family: "Font Awesome 5 Free";
    content: "\f133"
}

#progressbar #personal:before {
    font-family: "Font Awesome 5 Free";
    content: "\f007"
}

#progressbar #payment:before {
    font-family: "Font Awesome 5 Free";
    content: "\f030"
}

#progressbar #confirm:before {
    font-family: "Font Awesome 5 Free";
    content: "\f00c"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: 999
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #673AB7
}
#progressbar li.active:before,
#progressbar li.active:after {
    background: #673AB7
}
.progress {
    height: 20px
}
.progress-bar {
    background-color: #673AB7
}
</style>
@endpush

