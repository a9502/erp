@extends('layouts.admin')

@section('title', trans_choice('bulk-email::general.templates',2))

@section('new_button')
    <span>
        <a href="{{ route('bulk-email.templates.create') }}" class="btn btn-success btn-sm">{{ trans('general.add_new') }}</a>
    </span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header border-bottom-0" :class="[{'bg-gradient-primary': bulk_action.show}]">
            {!! Form::open([
                'method' => 'GET',
                'url' => 'bulk-email.templates.index',
                'role' => 'form',
                'class' => 'mb-0'
            ]) !!}
                <div class="align-items-center" v-if="!bulk_action.show">
                    <x-search-string model="Modules\BulkEmail\Models\Template" />
                </div>
                {{ Form::bulkActionRowGroup('bulk-email::general.templates', $bulk_actions, ['group' => 'bulk-email', 'type' => 'templates']) }}
            {!! Form::close() !!}
        </div>

        <div class="table-responsive">
            <table class="table table-flush table-hover">
                <thead class="thead-light">
                    <tr class="row table-head-line">
                        <th class="col-md-1">{{ Form::bulkActionAllGroup() }}</th>
                        <th class="col-md-5">@sortablelink('name', trans('general.name'))</th>
                        <th class="col-md-3">@sortablelink('type', trans('bulk-email::general.type'))</th>
                        <th class="col-md-2">@sortablelink('enabled', trans('general.enabled'))</th>
                        <th class="col-md-1">{{ trans('general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($templates as $item)
                        <tr class="row border-top-1">
                            <td class="col-md-1">
                                {{ Form::bulkActionGroup($item->id, $item->name) }}
                            </td>
                            <td class="col-md-5 border-0">
                                <a href="{{ route('bulk-email.templates.edit', $item->id) }}">{{ $item->name }}</a>
                            </td>
                            <td class="col-md-3 border-0">
                                {{ $item->type }}
                            </td>
                            <td class="col-md-2 border-0 hidden-xs">
                                {{ Form::enabledGroup($item->id, $item->name, $item->enabled) }}
                            </td>
                            <td class="col-md-1 border-0 hidden-xs text-nowrap">
                                <div class="dropdown">
                                    <a class="btn btn-neutral btn-sm text-light items-align-center p-2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h text-muted"></i>
                                    </a>
                                    
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="{{ route('bulk-email.templates.duplicate', $item->id) }}">{{ trans('general.duplicate') }}</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ route('bulk-email.templates.edit', $item->id) }}">{{ trans('general.edit') }}</a>
                                        
                                            <div class="dropdown-divider"></div>
                                            {!! Form::deleteLink($item,'bulk-email.templates.destroy') !!}
                                        
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer table-action">
            <div class="row align-items-center">
                @include('partials.admin.pagination', ['items' => $templates])
            </div>
        </div>

    </div>
@endsection

@push('scripts_start')
    <script src="{{ asset('js/modules/bulk-email/app.js?v=' . module_version('bulk-email')) }}"></script>
@endpush

