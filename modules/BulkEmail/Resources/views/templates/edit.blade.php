@extends('layouts.admin')

@section('title', trans('general.title.edit', ['type' => trans_choice('general.templates', 1)]))

@section('content')
    <div class="card">
        {!! Form::model($template,[
            'route' => ['bulk-email.templates.update',$template->id],
            'id' => 'bulk-email',
            'method' => 'PATCH',
            '@submit.prevent' => 'onSubmit',
            '@keydown' => 'form.errors.clear($event.target.name)',
            'files' => false,
            'role' => 'form',
            'class' => 'form-loading-button',
            'novalidate' => true
        ]) !!}

            <div class="card-body">
                <div class="row">
                    {{ Form::textGroup('name', trans('general.name'), 'font') }}
                    
                    {{ Form::textGroup('subject', trans('bulk-email::general.subject'), 'code') }}
                    
                    {{ Form::selectGroup('type', trans('bulk-email::general.type'), 'font', $types, $template->type, ['autocomplete' => 'off']) }}
                    
                    {{ Form::textareaGroup('body', trans('bulk-email::general.body'), 'code',$template->body,['rows' => '20','class' => 'tinymce']) }}
                    {{-- Form::tinymceEditorGroup('body', trans('bulk-email::general.body'),'font',$template->body) --}}

                    <div class="col-md-12">
                        <div class="bg-secondary border-radius-default border-1 p-2">
                            <small class="text-default">{!! trans('settings.email.templates.tags', ['tag_list' => config('setting.tag_templates')]) !!}</small>
                        </div>
                    </div>

                    {{ Form::radioGroup('enabled', trans('general.enabled'), $template->enabled) }}
                    
                </div>
            </div>

            <div class="card-footer">
                <div class="row save-buttons">
                    {{ Form::saveButtons('bulk-email.templates.index') }}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('scripts_start')
    <script src="{{ asset('js/modules/bulk-email/app.js?v=' . module_version('bulk-email')) }}"></script>
@endpush
