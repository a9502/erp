@extends('layouts.admin')

@section('title', trans('general.title.new', ['type' => trans_choice('bulk-email::general.templates', 1)]))

@section('content')
    <div class="card">
        {!! Form::open([
            'route' => 'bulk-email.templates.store',
            'id' => 'bulk-email',
            '@submit.prevent' => 'onSubmit',
            '@keydown' => 'form.errors.clear($event.target.name)',
            'files' => false,
            'role' => 'form',
            'class' => 'form-loading-button',
            'novalidate' => true
        ]) !!}

            <div class="card-body">
                <div class="row">
                    {{ Form::textGroup('name', trans('general.name'), 'font') }}

                    {{ Form::textGroup('subject', trans('bulk-email::general.subject'), 'code') }}

                    {{ Form::selectGroup('type', trans('bulk-email::general.type'), 'font', $types, '', ['autocomplete' => 'off']) }}
                    
                    {{ Form::textareaGroup('body', trans('bulk-email::general.body'),'font','',['rows' => '20','class' => 'tinymce']) }}

                    <div class="col-md-12">
                        <div class="bg-secondary border-radius-default border-1 p-2">
                            <small class="text-default">{!! trans('settings.email.templates.tags', ['tag_list' => config('setting.tag_templates')]) !!}</small>
                        </div>
                    </div>

                    {{ Form::radioGroup('enabled', trans('general.enabled'), true) }}
                    
                </div>
            </div>

            <div class="card-footer">
                <div class="row save-buttons">
                    {{ Form::saveButtons('bulk-email.templates.index') }}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('scripts_start')
    <script src="{{ asset('js/modules/bulk-email/app.js?v=' . module_version('bulk-email')) }}"></script>
@endpush


