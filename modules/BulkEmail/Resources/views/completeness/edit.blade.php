@extends('layouts.admin')

@section('title', trans('general.title.edit', ['type' => trans_choice('bulk-email::general.online_tests', 1)]))

@section('content')
    <div class="card">
        <div class="card-header wizard-header p-3">
            <ul id="progressbar">
                <li class="active" id="account"><strong>{{ __('bulk-email::general.schedule') }}</strong></li>
                <li  id="personal"><strong>{{ trans_choice('bulk-email::general.members',2) }}</strong></li>
            </ul>
        </div>

        {!! Form::model($completeness,[
            'method' => 'PATCH',
            'route' => ['bulk-email.completeness.update',$completeness->id],
            'id' => 'bulk-email',
            '@submit.prevent' => 'onSubmit',
            '@keydown' => 'form.errors.clear($event.target.name)',
            'files' => false,
            'role' => 'form',
            'class' => 'form-loading-button',
            'novalidate' => true
        ]) !!}

            <div class="card-body">
                <div class="row">
                    {{ Form::textGroup('name', trans('general.name'), 'font') }}
                    {{ Form::dateTimeGroup('schedule_at', trans('bulk-email::general.schedule_at'), 'calendar','',$completeness->schedule_at) }}
                    {{ Form::selectGroup('template_id', trans_choice('bulk-email::general.templates',1), 'code', $templates, $completeness->template_id, ['autocomplete' => 'on']) }}
                    {{ Form::radioGroup('enabled', trans('general.enabled'), $completeness->enabled) }}
                    
                </div>
            </div>

            <div class="card-footer">
                <div class="row save-buttons">
                    {{ Form::saveButtons('bulk-email.templates.index') }}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('scripts_start')
    <script src="{{ asset('js/modules/bulk-email/app.js?v=' . module_version('bulk-email')) }}"></script>
@endpush

@push('css')
<style>
#progressbar {
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey
}
#progressbar .active {
    color: #673AB7
}
#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 50%;
    float: left;
    position: relative;
    font-weight: 400;
    text-align: center
}

#progressbar #account:before {
    font-family: "Font Awesome 5 Free";
    content: "\f133"
}

#progressbar #personal:before {
    font-family: "Font Awesome 5 Free";
    content: "\f007"
}

#progressbar #payment:before {
    font-family: "Font Awesome 5 Free";
    content: "\f030"
}

#progressbar #confirm:before {
    font-family: "Font Awesome 5 Free";
    content: "\f00c"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: 999
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #673AB7
}
#progressbar li.active:before,
#progressbar li.active:after {
    background: #673AB7
}
.progress {
    height: 20px
}
.progress-bar {
    background-color: #673AB7
}
</style>
@endpush