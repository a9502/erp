<?php

namespace Modules\BulkEmail\BulkActions;

use App\Abstracts\BulkAction;
use Modules\BulkEmail\Models\BulkEmailInterview;
use Modules\BulkEmail\Jobs\Interview\DeleteInterview;

class Interviews extends BulkAction
{
    public $model = BulkEmailInterview::class;

    public $actions = [
        'delete' => [
            'name' => 'general.delete',
            'message' => 'bulk_actions.message.delete',
            'permission' => 'delete-employees-employees',
        ],
    ];

    public function destroy($request)
    {
        $items = $this->getSelectedRecords($request);

        foreach ($items as $item) {
            try {
                $this->dispatch(new DeleteInterview($item));
            } catch (\Exception $e) {
                flash($e->getMessage())->error()->important();
            }
        }
    }
}
