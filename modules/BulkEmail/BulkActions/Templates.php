<?php

namespace Modules\BulkEmail\BulkActions;

use App\Abstracts\BulkAction;
use Modules\BulkEmail\Models\Template;
use Modules\BulkEmail\Jobs\Template\DeleteTemplate;
use Modules\BulkEmail\Jobs\Template\UpdateTemplate;

class Templates extends BulkAction
{
    public $model = Template::class;

    public $actions = [
        'enable'  => [
            'name'       => 'general.enable',
            'message'    => 'bulk_actions.message.enable',
            'permission' => 'update-templates-templates',
        ],
        'disable' => [
            'name'       => 'general.disable',
            'message'    => 'bulk_actions.message.disable',
            'permission' => 'update-templates-templates',
        ],
        'delete' => [
            'name' => 'general.delete',
            'message' => 'bulk_actions.message.delete',
            'permission' => 'delete-templates-templates',
        ],
    ];

    public function destroy($request)
    {
        $templates = $this->getSelectedRecords($request);

        foreach ($templates as $template) 
        {
            try {
                $this->dispatch(new DeleteTemplate($template));
            } catch (\Exception $e) {
                flash($e->getMessage())->error()->important();
            }
        }
        $response['message'] = trans('messages.success.destroy', ['type' => $template->name]);
    }

    public function disable($request)
    {
        $templates = $this->getSelectedRecords($request);

        foreach ($templates as $template) 
        {
            try {
                $this->dispatch(new UpdateTemplate($template, ['enabled' => 0]));
            } catch (\Exception $e) {
                flash($e->getMessage())->error()->important();
            }
        }
        $response['message'] = trans('messages.success.disabled', ['type' => $template->name]);
    }

    public function enable($request)
    {
        $templates = $this->getSelectedRecords($request);

        foreach ($templates as $template) {
            try {
                $this->dispatch(new UpdateTemplate($template, ['enabled' => 1]));
            } catch (\Exception $e) {
                flash($e->getMessage())->error()->important();
            }
        }
    }
}
