<?php

namespace Modules\BulkEmail\BulkActions;

use App\Abstracts\BulkAction;
use Modules\BulkEmail\Models\Schedule as Test;
use Modules\BulkEmail\Jobs\Test\DeleteTest;

class Tests extends BulkAction
{
    public $model = Test::class;

    public $actions = [
        'enable' => [
            'name' => 'general.enable',
            'message' => 'bulk_actions.message.enable',
            //'permission' => 'update-employees-employees',
        ],
        'disable' => [
            'name' => 'general.disable',
            'message' => 'bulk_actions.message.disable',
            //'permission' => 'update-employees-employees',
        ],
        'delete' => [
            'name' => 'general.delete',
            'message' => 'bulk_actions.message.delete',
            //'permission' => 'delete-employees-employees',
        ],
    ];

    public function destroy($request)
    {
        $items = $this->getSelectedRecords($request);

        foreach ($items as $item) {
            try {
                $this->dispatch(new DeleteTest($item));
            } catch (\Exception $e) {
                flash($e->getMessage())->error()->important();
            }
        }
    }
}
