<?php

return [
    'Modules\BulkEmail\Models\Template' => [
        'columns' => [
            'id',
            'name'      => ['searchable' => true],
            'type' => [
                'values' => [
                    'Test' => 'Test',
                    'Interview' => 'Interview',
                    'Completeness' => 'Completeness'
                ],
            ],
            'enabled'   => ['boolean' => true],
        ],
    ],

    'Modules\BulkEmail\Models\Schedule' => [
        'columns' => [
            'id',
            'name' => ['searchable' => true],
            'total_member' => ['searchable' => true],
            'enabled'   => ['boolean' => true],
            'schedule_at' => ['date' => true],
        ],
    ],

    'Modules\BulkEmail\Models\Member' => [
        'columns' => [
            'id',
            'name'      => ['searchable' => true],
            'email'     => ['searchable' => true],
            'position'  => ['searchable' => true],
            'enabled'   => ['boolean' => true],
        ],
    ],

];
