<?php

use Illuminate\Support\Facades\Route;

/**
 * 'admin' middleware and 'my-blog' prefix applied to all routes (including names)
 *
 * @see \App\Providers\Route::register
 */

Route::admin('my-blog', function () {
    Route::get('/', 'Main@index');
});
