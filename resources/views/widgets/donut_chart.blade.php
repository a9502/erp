<div id="widget-{{ $class->model->id }}" class="col-md-6">
    <div class="card">
        @include($class->views['header'])

        <div class="card-body" id="widget-donut-{{ $class->model->id }}">
            <div class="chart-donut">
                {!! $chart->container() !!}
            </div>
        </div>
    </div>
</div>

@push('body_scripts')
    {!! $chart->script() !!}
@endpush
