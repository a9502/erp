/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./../../bootstrap');

 import Vue from 'vue';
 
 import DashboardPlugin from './../../plugins/dashboard-plugin';
 
 import Global from './../../mixins/global';
 
 import Form from './../../plugins/form';
 import BulkAction from './../../plugins/bulk-action';
 
 // plugin setup
 Vue.use(DashboardPlugin);
 
 const app = new Vue({
     el: '#app',
 
     mixins: [
        Global
     ],
 
     data: function () {
         return {
            form: new Form('report'),
            bulk_action: new BulkAction('reports'),
            report_fields: '',
            reports_total: [],
         }
     },
 
     created() {
        //console.log('reports :',reports_total);

         if (typeof reports_total !== 'undefined' && reports_total) {
             this.reports_total = reports_total;
         }
     },
 
     methods: {
      onRefreshTotal(report_id) {
        axios.get(url + '/common/reports/' + report_id + '/clear')
        .then(response => {
          console.log(response);
            this.reports_total[report_id] = response.data.data;
        })
        .catch(error => {
        });
      },
     }
 });
 