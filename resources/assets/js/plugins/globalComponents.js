import BaseInput from './../components/Inputs/BaseInput';
import Badge from './../components/Badge.vue';
import BaseButton from './../components/BaseButton.vue';
import TinyMceHtmlEditor from './../components/TinyMceHtmlEditor';

import { Input, Tooltip, Popover } from 'element-ui';

/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

 const GlobalComponents = {
  install(Vue) {
    Vue.component(Badge.name, Badge);
    Vue.component(BaseButton.name, BaseButton);
    Vue.component(BaseInput.name, BaseInput);
    Vue.component(Input.name, Input);
    Vue.component(TinyMceHtmlEditor.name, TinyMceHtmlEditor);
    Vue.use(Tooltip);
    Vue.use(Popover);
  }
 };

 export default GlobalComponents;