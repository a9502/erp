import Notifications from './../components/NotificationPlugin';

// A plugin file where you could register global components used across the app
import GlobalComponents from './globalComponents';

// asset imports
//import './../../sass/argon.scss';

export default {
  install(Vue) {
    Vue.use(Notifications);
    Vue.use(GlobalComponents);
  }
}