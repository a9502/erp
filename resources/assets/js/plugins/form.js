import Errors from './error';
import tinymce from 'tinymce';

export default class Form {
  constructor(form_id) {

    //tinymce.triggerSave();

    
    //return;

    let form = document.getElementById(form_id);

    //console.log('textarea :' ,form.getElementsByTagName("textarea"))

    if (!form) {
      return;
    }

    this['method'] = form.getAttribute('method').toLowerCase();
    this['action'] = form.getAttribute('action');

    for (let form_element of document.getElementById(form_id).getElementsByTagName("input")) {
      if (form_element.getAttribute('id') == 'global-search') {
          continue;
      }

      var name = form_element.getAttribute('name');
      var type = form_element.getAttribute('type');

      console.log('input :',name);

      if (name == 'method') {
          continue;
      }

      
      if (form_element.getAttribute('data-item')) {
          if (!this['items']) {
              var item = {};
              var row = {};

              item[0] = row;
              this['items'] = item;
          }

          if (!this['items'][0][form_element.getAttribute('data-item')]) {
              this['items'][0][form_element.getAttribute('data-item')] = '';
          }

          this['item_backup'] = this['items'];

          continue;
      }

      if (form_element.getAttribute('data-field')) {
          if (!this[form_element.getAttribute('data-field')]) {
              var field = {};

              this[form_element.getAttribute('data-field')] = field;
          }

          if (type == 'radio') {
              if (!this[form_element.getAttribute('data-field')][name]) {
                  this[form_element.getAttribute('data-field')][name] = (form_element.getAttribute('value') ? form_element.getAttribute('value') : 0) || 0;
              } else if (form_element.checked) {
                  this[form_element.getAttribute('data-field')][name] = (form_element.getAttribute('value') ? form_element.getAttribute('value') : 0) || 0;
              } else if (form_element.getAttribute('checked')) {
                  this[form_element.getAttribute('data-field')][name] = (form_element.getAttribute('value') ? form_element.getAttribute('value') : 0) || 0;
              }
          } else if (type == 'checkbox') {
              if (this[form_element.getAttribute('data-field')][name]) {
                  if (!this[form_element.getAttribute('data-field')][name].push) {
                      this[form_element.getAttribute('data-field')][name] = [this[form_element.getAttribute('data-field')][name]];
                  }

                  if (form_element.checked) {
                      this[form_element.getAttribute('data-field')][name].push(form_element.value);
                  }
              } else {
                  if (form_element.checked) {
                      if (form_element.dataset.type != undefined) {
                          if (form_element.dataset.type == 'multiple') {
                              this[name] = [];

                              this[form_element.getAttribute('data-field')][name].push(form_element.value);
                          } else {
                              this[form_element.getAttribute('data-field')][name] = form_element.value;
                          }
                      } else {
                          this[form_element.getAttribute('data-field')][name] = form_element.value;
                      }
                  } else {
                      this[form_element.getAttribute('data-field')][name] = [];
                  }
              }
          } else {
              this[form_element.getAttribute('data-field')][name] = form_element.getAttribute('value') || '';
          }

          continue;
      }

      if (type == 'radio') {
          if (!this[name]) {
              this[name] = (form_element.getAttribute('value') ? form_element.getAttribute('value') : 0) || 0;
          } else if (form_element.checked) {
              this[name] = (form_element.getAttribute('value') ? form_element.getAttribute('value') : 0) || 0;
          } else if (form_element.getAttribute('checked')) {
              this[name] = (form_element.getAttribute('value') ? form_element.getAttribute('value') : 0) || 0;
          }
      } else if (type == 'checkbox') {
          if (this[name]) {
              if (!this[name].push) {
                  this[name] = [this[name]];
              }

              if (form_element.checked) {
                  this[name].push(form_element.value);
              }
          } else {
              if (form_element.checked) {
                  if (form_element.dataset.type != undefined) {
                      if (form_element.dataset.type == 'multiple') {
                          this[name] = [];

                          this[name].push(form_element.value);
                      } else {
                          this[name] = form_element.value;
                      }
                  } else {
                      this[name] = form_element.value;
                  }
              } else {
                  this[name] = [];
              }
          }
      } else {
          this[name] = form_element.getAttribute('value') || '';
      }
    }

    for (let form_element of document.getElementById(form_id).getElementsByTagName("textarea")) {
        var name = form_element.getAttribute('name');

        console.log('textarea :',name);

        

        if (name == 'method') {
            continue;
        }

        if (form_element.getAttribute('data-item')) {
            if (!this['items']) {
                var item = {};
                var row = {};

                item[0] = row;
                this['items'] = item;
            }

            if (!this['items'][0][form_element.getAttribute('data-item')]) {
                this['items'][0][form_element.getAttribute('data-item')] = '';
            }

            this['item_backup'] = this['items'];

            continue;
        }

        if (form_element.getAttribute('data-field')) {
            if (!this[form_element.getAttribute('data-field')]) {
                var field = {};

                this[form_element.getAttribute('data-field')] = field;
            }

            if (!this[form_element.getAttribute('data-field')][name]) {
                this[form_element.getAttribute('data-field')][name] = '';
            }

            continue;
        }

        if (this[name]) {
            if (!this[name].push) {
                this[name] = [this[name]];
            }

            this[name].push(form_element.value || '');
        } else {
            this[name] = form_element.value || '';
        }
    }

    for (let form_element of document.getElementById(form_id).getElementsByTagName("select")) {
        var name = form_element.getAttribute('name');

        if (name == 'method') {
            continue;
        }

        if (form_element.getAttribute('data-item')) {
            if (!this['items']) {
                var item = {};
                var row = {};

                item[0] = row;
                this['items'] = item;
            }

            if (!this['items'][0][form_element.getAttribute('data-item')]) {
                this['items'][0][form_element.getAttribute('data-item')] = '';
            }

            this['item_backup'] = this['items'];

            continue;
        }

        if (form_element.getAttribute('data-field')) {
            if (!this[form_element.getAttribute('data-field')]) {
                var field = {};

                this[form_element.getAttribute('data-field')] = field;
            }

            if (!this[form_element.getAttribute('data-field')][name]) {
                this[form_element.getAttribute('data-field')][name] = '';
            }

            continue;
        }

        if (this[name]) {
            if (!this[name].push) {
                this[name] = [this[name]];
            }

            this[name].push(form_element.getAttribute('value') || '');
        } else {
            this[name] = form_element.getAttribute('value') || '';
        }
    }

    this.errors = new Errors();
    this.loading = false;
    this.response = {};
  }

  data() {
    let data = Object.assign({}, this);
    delete data.method;
    delete data.action;
    delete data.errors;
    delete data.loading;
    delete data.response;
    return data;
  }

  submit() {  
    //console.log('content :', tinyMCE.activeEditor.getContent());
    //$('.tinymce').val(tinyMCE.activeEditor.getContent());
  

    FormData.prototype.appendRecursive = function(data, wrapper = null) {  
      for (var name in data) {
          if (name == "previewElement" || name == "previewTemplate") {
              continue;
          }

          console.log('object :',name);

          if (wrapper) {
              if ((typeof data[name] == 'object' || Array.isArray(data[name])) && ((data[name] instanceof File != true ) && (data[name] instanceof Blob != true))) {
                  this.appendRecursive(data[name], wrapper + '[' + name + ']');
              } else {
                  this.append(wrapper + '[' + name + ']', data[name]);
              }
          } else {
              if ((typeof data[name] == 'object' || Array.isArray(data[name])) && ((data[name] instanceof File != true ) && (data[name] instanceof Blob != true))) {
                  this.appendRecursive(data[name], name);
              } else {
                  console.log('name text :',name);
                  console.log('name value :',data[name]);
                  if(name == 'body') {
                    this.append(name, tinyMCE.activeEditor.getContent());
                  } else {
                    this.append(name, data[name]);
                    
                  }
                  
              }
          }
      }
    };

    //console.log('okay');
    this.loading = true;
    let data = this.data();
    //console.log('data :',data);
    let form_data = new FormData();
    form_data.appendRecursive(data);
    //console.log('data :',form_data);

    window.axios({
      method: this.method,
      url: this.action,
      data: form_data,
      headers: {
          'X-CSRF-TOKEN': window.Laravel.csrfToken,
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'multipart/form-data'
      }
    })
    .then(this.onSuccess.bind(this))
    .catch(this.onFail.bind(this));
    //.finally(this.onFinal.bind(this));
    //.then(this.onFinal.bind(this));
  }

  onSuccess(response) {
    this.errors.clear();

    this.loading = false;

    if (response.data.redirect) {
        this.loading = true;

        window.location.href = response.data.redirect;
    }

    this.response = response.data;
  }

  // Form fields check validation issue
  onFail(error) {
    this.loading = false;
    this.errors.record(error.response.data.errors);
    
  }

  // Form fields check validation issue
  onFinal() {
    console.log('errorx :');
    this.errors.clear();
    this.loading = false;
  }


}
