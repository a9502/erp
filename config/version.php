<?php

return [

    'name'      => 'Akaunting',
    'code'      => 'Document',
    'major'     => '1',
    'minor'     => '0',
    'patch'     => '1',
    'build'     => '',
    'status'    => 'Stable',
    'date'      => '10-August-2017',
    'time'      => '15:30',
    'zone'      => 'GMT +7'
];
