<?php

use Illuminate\Support\Facades\Route;

/**
 * 'wizard' middleware and prefix applied to all routes
 *
 * @see \App\Providers\Route::mapWizardRoutes
 */

Route::group(['as' => 'wizard.'], function () {
    //Route::get('companies', 'Wizard\Companies@edit')->name('companies.edit');
    Route::post('companies', 'Wizard\Companies@update')->middleware([])->name('companies.update');
    Route::patch('finish', 'Wizard\Finish@update')->name('finish.update');
});