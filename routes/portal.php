<?php

use Illuminate\Support\Facades\Route;

/**
 * 'portal' middleware and prefix applied to all routes
 *
 * @see \App\Providers\Route::mapPortalRoutes
 * @see \modules\OfflinePayments\Routes\portal.php for module example
 */

Route::group(['as' => 'portal.'], function () {
    Route::resource('invoices', 'Portal\Invoices');
    Route::resource('payments', 'Portal\Payments');
    Route::resource('profile', 'Portal\Profile', ['middleware' => ['dropzone']]);
    Route::get('logout', 'Auth\Login@destroy')->name('logout');
});